import matplotlib.pyplot as plt
import csv 

datafilename = ["training-1","training-2"]

for name in datafilename:
    ucpu = []
    umem = []
    tcpu = []
    tmem = []
    t = []
    i = 0
    csvname = "analysis//usenumof" + name + ".csv"
    with open(csvname) as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            ucpu.append(int(row[1]))
            umem.append(int(row[3]))
            tcpu.append(int(row[5]))
            tmem.append(int(row[7]))
            t.append(i)
            i = i + 1
    fig = plt.figure()
    plt.plot(t, ucpu, label="requiredCPU")
    plt.plot(t, tcpu,label="purchasedCPU")
    plt.plot(t, umem,label="requiredMEM")
    plt.plot(t, tmem,label="purchasedMEM")
    plt.legend()
    # plt.show()
    plt.savefig("analysisVisualization//usedsource of "+ name + ".jpg")