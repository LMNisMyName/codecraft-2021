#pragma once
#include <string>
#include <list>
#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include "vm.hpp"

class Vm;
class Vmbase;

struct Compare
{
    bool operator()(const Vm *vmL, const Vm *vmR);
};
class Serverbase //服务器基类，包含基本信息
{                //其对象为尚未购买的服务器
public:
    Serverbase() : name("none"), core(0), mem(0), hdcost(0), elecost(0) {} //default constructor
    Serverbase(std::string name, const int core, const int mem, const int hdcost, const int elecost);
    void print();                   //打印当前服务器基本信息
    std::string name;               //服务器名称
    int core, mem, hdcost, elecost; //服务器的硬件和成本信息
};

class Server : public Serverbase //服务器类
{                                //其对象为数据中心内的服务器
public:
    Server() {}                            //default constructor
    Server(Serverbase *);                  //由基类对象构造服务器对象
    std::vector<std::pair<int, char>> vms; //保存服务器内所存的虚拟机的ID和占用节点

    int id = -1;                                                //服务器id(初始值为-1)
    int usedCpuA = 0, usedMemA = 0, usedCpuB = 0, usedMemB = 0; //服务器硬件使用数
    double paycore = 0., paymem = 0.;                           //存储服务器硬件使用率
    double cpurate = 0., memrate = 0.;                          //存储服务器硬件使用率
    int rank = 0;                                               //购买时的id
    bool TurnOnflag = 0;                                        //当天是否打开
    void usagePrint();                                          //用于调试，打印服务器硬件使用率
    void usagePrint(std::ofstream &);
    bool empty();
    char ableToadd(Vm *vm);
    std::istream &operator>>(std::istream &input);
    std::multiset<Vm *, Compare> vmMembers; //保存服务器内具有的虚拟机(按照大小排序)
};

/* 根据当天请求的CPU和MEM的数量，选择最合适的Server种类
    vecSv: 可以购买到的Serverbase的列表
    numSv: 可以购买到的Serverbase的数量
    cpuNeed: 当天请求需要的CPU的数量
    memNeed: 当天请求需要的MEM的数量
    deadSpcae: 死空间的大小，是一个可调节参数
*/
Serverbase *selectBestServer(Serverbase *vecSv, int numSv, int cpuNeed, int memNeed,
                             int deadSpace, int dayLeft);
Serverbase *selectBestServer_v2(Serverbase *vecSv, int numSv, int cpuNeed, int memNeed, int deadSpace, int dayLeft);
