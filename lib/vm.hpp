#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include "server.hpp"

class Server;

class Vmbase //虚拟机基类
{            //表示基本虚拟机
public:
    Vmbase() : name("none"), core(0), mem(0), node(0) {}
    Vmbase(std::string name, const int core, const int mem, const int node);
    void print(); //打印虚拟机基本信息
    std::string name;
    int core, mem, node;
};

class Vm : public Vmbase //表示数据中心内的虚拟机
{
public:
    Vm() {}
    Vm(Vmbase *);
    Vm(Vmbase *base, int vmId);
    int vmId;
    std::pair<Server *, char> svm; //该虚拟机所在的服务器地址和节点
};
