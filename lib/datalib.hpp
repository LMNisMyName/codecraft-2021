#pragma once
#include <list>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include "req.hpp"
#include "server.hpp"
#include "vm.hpp"

struct ReqCompare
{
    bool operator()(const Req *reqL, const Req *reqR);
};

class Datalib
{
public:
    Datalib()
    {
        svnum = 0;
        vmnum = 0;
    }                                                         //默认构造函数，初始化数据中心内服务器和虚拟机的数目为零
    bool del(int vmId);                                       //删除数据中心内的一个ID为vmID的虚拟机
    void addvm(Vm *vm, int vmId, Server *server, const char); //添加一台指定型号的虚拟机到数据中心服务器的指定节点
                                                              //vm：指向该虚拟机的指针，vmID：虚拟机编号，server：目标服务器指针，char：目标节点
                                                              //"A":A节点， "B":B节点，"X":X节点
    bool addSuccess(Vmbase *vmbase, int vmId);                //判断当前虚拟机是否可以添加到数据中心内
    bool addSuccess_v2(Vmbase *vmbase, int vmId);             //判断当前虚拟机是否可以添加到数据中心内
                                                              //vmbase:要添加的虚拟机，vmId:用户虚拟机ID；var:优化参数；调控虚拟机位置
    bool addSuccess_v3(Vmbase *vmbase, int vmId);
    void purchase(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int remainDays);
    void purchaseBestSv(Vmbase *base, int vmId, Serverbase *serverBest);
    void purchasebyUsenum(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int remainDays);
    void purchasebyuser(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int remainDays);
    void purchasebyuserInoneweek(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int remainDays);
    void purchase_MQ(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int day);
    void migrate();
    void addvmformig(Vm *vmmig, Server *sv, const char nodetype);
    void delformig(Vm *vmmig);
    //根据当前请求的虚拟机购买一台的服务器（虚拟机不能把当前数据中心容纳）
    void logToday(std::ostringstream &out);            //输出当天的购买服务器、迁移虚拟机和添加虚拟机到服务器信息
                                                       //同时给当天购买的服务器赋予一个ID
    void initial();                                    //初始化，清除上一天的打印信息
    double matchDegree(Vm *, Server *, char nodetype); //用户添加的虚拟机与服务器的匹配度
    void logSvusage();                                 //用于调试：输出当前数据中心内所有服务器的占用率
    void logSvusage(std::ofstream &);
    void logSvusenum(std::ofstream &);
    void logUsedServernumber(std::ofstream &fout);
    char abletoAdd(std::list<Server>::iterator it, Vm *newVm);
    bool caseDeadspace(std::list<Server>::iterator it, char nodetoAdd, Vm *newVm);
    bool migAvm(Vm *vmcur);
    bool migAvm(Vm *, int depth);
    bool add_migsuccess(Vmbase *vmbase, int vmId, int migdepth);
    void migvm2sv(Vm *vm, Server *sv, char node);
    void purchasebyuser(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int remainDays, double rate);
    void mergevm();
    void mig2mig(Vm *vmmig, Vm *vmcur);
    bool migAvm3(Vm *vmcur);
    void printTotalCost();
    void calTotalHdcost();
    void calElcostOneday();
    void ifTurnOn();
    bool migAvmFront(Vm *vmcur);
    void balance();
    bool migAvmNotEmpty(Vm *vmcur);
    void coutUseRate();
    void printTotalCost(std::ofstream &out);
    void foutUseRate(std::ofstream &out);
    void migrateTailToHead(int mode = 1);
    bool tryMigrateVmToSv(std::set<Vm *>::iterator &vmmig, Server *sv);
    bool tryMigrateVmToSvSecond(std::multiset<Vm *>::iterator &vmmig, Server *sv);
    bool migAble();
    std::pair<Server *, char> addvmNew(Vm *newVm, int vmId, Server *sv, const char nodetype);
    bool addExistServerSuccess(Req *req);
    bool addSvTodaySuccess(Req *req);
    void purchasebyuserToday(Req *req, Serverbase *serverLib, int serverNum, int remainDays, double rate);
    //private:
    static std::map<std::string, Vmbase *> vmNameArray;
    std::list<Server> serverArray;                                        //服务器列表
    int svnum = 0;                                                        //服务器总数
    std::list<Vm> vmArray;                                                //虚拟机列表
    int vmnum = 0;                                                        //虚拟机总数
    std::unordered_map<int, Vm *> vmMap;                                  //根据虚拟机的Id查询虚拟机地址
    std::vector<std::string> logpurMessage;                               //某天的服务器购买信息（服务器名字）
    std::vector<Server *> psa;                                            //某天购买的服务器在服务器列表中的地址
    std::vector<std::pair<int, std::pair<Server *, char>>> logmigMessage; //某天的虚拟机迁移信息
    std::vector<std::pair<Server *, char>> logaddMessage;                 //某天的虚拟机添加信息（添加到的服务器地址，节点）
    int purNum, migNum, addNum;
    int svid = -1;
    int rundays = -1;
    const int CPUDEAD = 10;  //5;
    const int MEMDEAD = 15;  //8;
    const int CPUALIVE = 30; //15;
    const int MEMALIVE = 45; //30;
    int vmnumToday = 0;      //保存当天操作执行之前的虚拟机总数

    //double totcpuRate = 0., totmemRate = 0.;              //当前数据中心总的硬件使用率
    int totCpu = 0, totMem = 0, usedCpu = 0, usedMem = 0; //当前数据中心总的硬件和已经使用的硬件
    const double THRESHOLD1 = 1;
    const double THRESHOLD2 = 0.90;
    const double THRESHOLD3 = 0.5;
    const double THRESHOLDDELTA = 0.30;
    const double THRESHOLDAVER = 0.7;
    int totalHdcost = 0;
    int totalElcost = 0;
    int cpuNeedToday = 0;
    int memNeedToday = 0;
    std::vector<Vm *> vmArrayToday;                    //保存当天新请求的虚拟机
    std::list<Req> reqArrayToday;                      //保存当天的请求
    std::multiset<Req *, ReqCompare> purchaseSetToday; //保存当天要购买的请求,未被响应的add请求
    std::vector<Req *> delReqToday;                    //保存当天未被响应的del请求
    std::list<Server> newServerToday;
    std::list<Server *> perfectServerLists; //保存服务器列表中已经完美的服务器
    const double INF = 1000000000.;
};
