#pragma once
#include "server.hpp"
#include "vm.hpp"
#include "req.hpp"
#include <fstream>

std::ifstream &operator>>(std::ifstream &input, Serverbase &server);
std::ifstream &operator>>(std::ifstream &input, Vmbase &vm);
std::ifstream &operator>>(std::ifstream &input, Req &req);

std::ifstream &operator>>(std::ifstream &input, Serverbase &server)
{
    std::string name;
    int core, mem, hdcost, elecost;
    char temp;
    input >> temp >> name >> core >> temp >> mem >> temp >> hdcost >> temp >> elecost >> temp;
    name.pop_back();
    Serverbase serverTemp(name, core, mem, hdcost, elecost);
    server = serverTemp;
    return input;
}
std::ifstream &operator>>(std::ifstream &input, Vmbase &vm)
{
    std::string name;
    int core, mem, node;
    char temp;
    input >> temp >> name >> core >> temp >> mem >> temp >> node >> temp;
    name.pop_back();
    Vmbase vmTemp(name, core, mem, node);
    vm = vmTemp;
    return input;
}

std::ifstream &operator>>(std::ifstream &input, Req &req)
{
    char temp;
    input >> temp >> req.mode;
    req.mode.pop_back();
    if (req.mode == std::string("add"))
    {
        input >> req.vmName >> req.vmId >> temp;
        req.vmName.pop_back();
    }
    else
        input >> req.vmId >> temp;
    return input;
}