#pragma once
#include <string>
#include <fstream>
#include "server.hpp"
struct Req
{
    std::string mode;              //请求类型：add或者del
    std::string vmName;            //请求的虚拟机的名字
    int vmId;                      //请求的虚拟机的id
    std::pair<Server *, char> ack; //当前请求对于的响应
    //int day;       //请求之后剩余的时间（总时间-当前请求发生的时间）
};
