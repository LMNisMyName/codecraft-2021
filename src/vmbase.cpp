#include <iostream>
#include "vm.hpp"
#include <string>
#include <fstream>

void Vmbase::print()
{
    std::cout << name << "," << core << "," << mem << "," << node << std::endl;
}
Vmbase::Vmbase(std::string name, const int core, const int mem, const int node)
{
    this->name = name;
    this->core = core;
    this->mem = mem;
    this->node = node;
}

Vm::Vm(Vmbase *base)
{
    this->name = base->name;
    this->core = base->core;
    this->mem = base->mem;
    this->node = base->node;
}

Vm::Vm(Vmbase *base, int vmId)
{
    this->name = base->name;
    this->core = base->core;
    this->mem = base->mem;
    this->node = base->node;
    this->vmId = vmId;
}
