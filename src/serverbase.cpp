#include <iostream>
#include <string>
#include <fstream>
#include "server.hpp"

void Serverbase::print()
{
    std::cout << name << "," << core << "," << mem << "," << hdcost << "," << elecost << std::endl;
}

Serverbase::Serverbase(std::string name, const int core, const int mem, const int hdcost, const int elecost)
{
    this->name = name;
    this->core = core;
    this->mem = mem;
    this->hdcost = hdcost;
    this->elecost = elecost;
}

Server::Server(Serverbase *base)
{
    this->name = base->name;
    this->core = base->core;
    this->mem = base->mem;
    this->hdcost = base->hdcost;
    this->elecost = base->elecost;
}

void Server::usagePrint()
{
    std::cout << "usage = [" << 2 * usedCpuA / core << ", " << 2 * usedMemA / core << ", "
              << 2 * usedCpuB / core << ", " << 2 * usedMemB / core << "]\n";
}
void Server::usagePrint(std::ofstream &out)
{
    out << "serverID:" << id << ": usage = [" << 2. * usedCpuA / core << ", " << 2. * usedMemA / mem << ", "
        << 2. * usedCpuB / core << ", " << 2. * usedMemB / mem << "]\n";
    out << "[servername:" << name << ", core:" << this->core << ", mem" << this->mem << ", hdcost"
        << this->hdcost << ", elecost" << this->elecost << "]\n";
}

Serverbase *selectBestServer(Serverbase *vecSv, int numSv, int cpuNeed, int memNeed, int deadSpace, int dayLeft)
{
    int indexBest = 0;
    int priceBest = INT32_MAX;
    for (int i = 0; i < numSv; i++)
    {
        Serverbase sv = vecSv[i];
        int numNeedForCpu = cpuNeed / (sv.core - deadSpace) + 1;
        int numNeedForMem = memNeed / (sv.mem - deadSpace) + 1;
        int numNeed = std::max(numNeedForCpu, numNeedForMem);
        int price = numNeed * (sv.hdcost + sv.elecost * dayLeft);
        if (price < priceBest)
        {
            priceBest = price;
            indexBest = i;
        }
    }
    return (vecSv + indexBest);
}

Serverbase *selectBestServer_v2(Serverbase *vecSv, int numSv, int cpuNeed, int memNeed, int deadSpace, int dayLeft)
{
    int indexBest = 0;
    int priceBest = INT32_MAX;
    double rate = double(cpuNeed) / memNeed;
    int memUable = 0, cpuUable = 0;
    for (int i = 0; i < numSv; i++)
    {
        Serverbase sv = vecSv[i];
        cpuUable = std::min(sv.core, int(sv.mem * rate));
        memUable = std::min(sv.mem, int(sv.core / rate));
        int price = (sv.hdcost + sv.elecost * dayLeft) / (cpuUable + memUable);
        if (price < priceBest)
        {
            priceBest = price;
            indexBest = i;
        }
    }
    return (vecSv + indexBest);
}

bool Server::empty()
{
    if (vms.size() == 0)
        return true;
    else
        return false;
}
char Server::ableToadd(Vm *vm)
{
    if (empty())
        return 0;
    if (vm->node == 1)
    {
        if ((vm->core / 2 + usedCpuA) <= (core / 2) &&
            (vm->core / 2 + usedCpuB) <= (core / 2) &&
            (vm->mem / 2 + usedMemA <= mem / 2) &&
            (vm->mem / 2 + usedMemB <= mem / 2))
            return 'X';
        else
            return 0;
    }
    else
    {
        if ((vm->core + usedCpuA <= core / 2) &&
            (vm->mem + usedMemA <= mem / 2))
            return 'A';
        else if ((vm->core + usedCpuB <= core / 2) &&
                 (vm->mem + usedMemB <= mem / 2))
            return 'B';
        else
            return 0;
    }
}

bool Compare::operator()(const Vm *vmL, const Vm *vmR)
{
    return (vmL->core + vmL->mem) < (vmR->core + vmR->mem);
}