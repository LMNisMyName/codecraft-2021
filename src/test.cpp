#define ANALYSIS
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <fstream>
#include <ctime>
#include <queue>
//#include "reqList.hpp"
#include "vm.hpp"
#include "server.hpp"
#include "req.hpp"
#include "datalib.hpp"
#include "ifstreamInputOperator.hpp"
using std::ifstream;
using std::ofstream;

int main(int argc, char *argv[])
{
    int totalCost = 0;
    for (int argCnt = 1; argCnt < argc; argCnt++)
    {
        //记录程序开始运行的时间
        time_t Start_time;
        Start_time = time(nullptr);
        //读入基本类型的服务器和虚拟机
        ifstream input;
        std::string ifilename = argv[argCnt];
        input.open("traindata//" + ifilename + ".txt");
        int serverNum, vmNum;
        input >> serverNum;
        Serverbase *serverLib = new Serverbase[serverNum];
        for (int i = 0; i < serverNum; i++)
        {
            input >> serverLib[i];
        }
        input >> vmNum;
        Vmbase *vmLib = new Vmbase[vmNum];
        for (int i = 0; i < vmNum; i++)
        {
            input >> vmLib[i];
            Datalib::vmNameArray[vmLib[i].name] = vmLib + i;
        }
        //读入数据中心运行的总天数
        int totalDay;
        input >> totalDay;
#ifdef ANALYSIS
        /*将统计量输出到文件
     *usagefile : 每台服务器硬件资源的使用率
     *usenumfile ：每台服务器硬件资源的使用量
     *migfile ：当天的迁移数目和比例
     *useratefile : 当天数据中心总硬件资源的使用率*/
        ofstream usagefile;
        usagefile.open("analysis//usagefileof" + ifilename + ".txt");
        ofstream usenumfile;
        usenumfile.open("analysis//usenumof" + ifilename + ".csv");
        ofstream migfile;
        migfile.open("analysis//migof" + ifilename + ".txt");
        ofstream useratefile;
        useratefile.open("analysis//userateof" + ifilename + ".txt");
#endif
        std::ostringstream out; //保存每天执行的添加、购买和迁移操作
        Datalib myDatalib;
        //以天为单位进行虚拟机的迁移，虚拟机的添加，服务器的购买
        for (int day = 0; day < totalDay; day++)
        {

            myDatalib.initial();
#ifdef ANALYSIS
            myDatalib.ifTurnOn(); //方便后续计算电费
#endif
            //执行虚拟机迁移操作
            myDatalib.mergevm();
            //myDatalib.balance();
            //myDatalib.migrate();
            //myDatalib.migrateTailToHead(1);

            /*逐条执行虚拟机添加和删除请求
            *未成功的添加请求保存有序集合purchaseSetToday
            *未成功的删除请求保存到delReqToday*/
            int reqNum;
            input >> reqNum;
            for (int cnt = 0; cnt < reqNum; cnt++)
            {
                Req req;
                input >> req;
                myDatalib.reqArrayToday.push_back(req);
                if (req.mode != std::string("del"))
                {
                    Vm *newvmToday = new Vm(Datalib::vmNameArray[req.vmName], req.vmId);
                    myDatalib.vmArrayToday.push_back(newvmToday);
                    if (!myDatalib.addExistServerSuccess(&myDatalib.reqArrayToday.back()))
                    {
                        myDatalib.purchaseSetToday.insert(&myDatalib.reqArrayToday.back());
                        myDatalib.cpuNeedToday += Datalib::vmNameArray[req.vmName]->core;
                        myDatalib.memNeedToday += Datalib::vmNameArray[req.vmName]->mem;
                    }
                }
                else
                {
                    if (!myDatalib.del(req.vmId))
                    {
                        myDatalib.delReqToday.push_back(&myDatalib.reqArrayToday.back());
                    }
                }
            }
            /*计算未添加成功的虚拟机所需的cpu和mem的比例
         *根据此比例购买成本最低的服务器*/
            double rate = 1.;
            rate = double(myDatalib.cpuNeedToday) / myDatalib.memNeedToday;
            for (auto reqIt = myDatalib.purchaseSetToday.begin(); reqIt != myDatalib.purchaseSetToday.end(); reqIt++)
            {
                if (!myDatalib.addSvTodaySuccess(*reqIt))
                {
                    myDatalib.purchasebyuserToday(*reqIt, serverLib, serverNum, totalDay - day, rate);
                }
            }
            myDatalib.serverArray.splice(myDatalib.serverArray.end(), myDatalib.newServerToday);
            //处理未成功的删除请求
            for (auto reqIt = myDatalib.delReqToday.begin(); reqIt != myDatalib.delReqToday.end(); reqIt++)
            {
                myDatalib.del((*reqIt)->vmId);
            }

#ifdef ANALYSIS
            myDatalib.calElcostOneday();
            //std::cout << day << "day has passed\n"; //估计程序运行时间
            myDatalib.logToday(out);
            myDatalib.logSvusenum(usenumfile);
            myDatalib.foutUseRate(useratefile);
            migfile << '(' << "migrationNum: "
                    << ", " << myDatalib.migNum << ", ratio:"
                    << double(myDatalib.migNum) / myDatalib.vmnumToday
                    << ')' << "\n";
#endif
        }
        //myDatalib.coutUseRate();
        myDatalib.printTotalCost();
        totalCost += myDatalib.totalElcost + myDatalib.totalHdcost;
        time_t end_time;
        end_time = time(nullptr);
        std ::cout << "runtime on " + std::string(argv[argCnt]) + " = " << end_time - Start_time << "s\n";
#ifdef ANALYSIS
        ofstream request;
        request.open("analysis//request.txt");
        request << out.str();
        myDatalib.logSvusage(usagefile);
        usagefile.close();
        request.close();
#endif
    }
    std::cout << "totalCost = " << totalCost << std::endl;
    return 0;
}