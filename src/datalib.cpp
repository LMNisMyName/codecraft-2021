#include "datalib.hpp"
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <math.h>
#include <stack>
using std::cout;

std::map<std::string, Vmbase *> Datalib::vmNameArray = {};

void Datalib::initial() //数据库需要打印的操作信息每日进行初始化
{
    purNum = 0, migNum = 0, addNum = 0;
    cpuNeedToday = 0;
    memNeedToday = 0;
    rundays++;
    vmnumToday = vmnum;
    logpurMessage.clear();
    logmigMessage.clear();
    logaddMessage.clear();
    psa.clear();
    vmArrayToday.clear();
    newServerToday.clear();
    purchaseSetToday.clear();
    delReqToday.clear();
    reqArrayToday.clear();
}

void Datalib::logToday(std::ostringstream &out) //输出当天的购买服务器、迁移虚拟机和添加虚拟机到服务器信息
{                                               //同时给当天购买的服务器赋予一个id
    std::vector<std::string> lpM = logpurMessage;
    std::sort(lpM.begin(), lpM.end());
    lpM.erase(std::unique(lpM.begin(), lpM.end()), lpM.end());
    out << '(' << "purchase"
        << ", " << lpM.size() << ')' << "\n";
    for (size_t i = 0; i < lpM.size(); i++)
    {
        int cnt = 0; //对服务器类型进行计数
        for (size_t j = 0; j < psa.size(); j++)
        {
            if (psa[j]->name == lpM[i])
            {
                svid += 1;
                psa[j]->id = svid;
                cnt++;
            }
        }
        out << '(' << lpM[i] << ", " << cnt << ')' << "\n";
    }
    out << '(' << "migration"
        << ", " << migNum << ')' << "\n";
    for (int i = 0; i < migNum; i++)
    {
        if (logmigMessage[i].second.second != 'X')
            out << '(' << logmigMessage[i].first << ", " << logmigMessage[i].second.first->id << ", " << logmigMessage[i].second.second << ")\n";
        else
            out << '(' << logmigMessage[i].first << ", " << logmigMessage[i].second.first->id << ")\n";
    }

    //遍历reqArrayToday，输出add信息
    for (auto reqIt = reqArrayToday.begin(); reqIt != reqArrayToday.end(); reqIt++)
    {
        if (reqIt->mode == std::string("add"))
        {
            Req debug = *reqIt;
            if (reqIt->ack.second == 'X')
                out << '(' << reqIt->ack.first->id << ')' << '\n';
            else
                out << '(' << reqIt->ack.first->id << ", " << reqIt->ack.second << ')' << '\n';

            if (reqIt->ack.second == 'A')
                out << '(' << reqIt->ack.first->id << ", " << reqIt->ack.second << ')' << '\n';
            if (reqIt->ack.second == 'B')
                out << '(' << reqIt->ack.first->id << ", " << reqIt->ack.second << ')' << '\n';
        }
    }
}

void Datalib::logSvusage()
{
    for (auto it = serverArray.begin(); it != serverArray.end(); it++)
    {
        it->usagePrint();
    }
}

void Datalib::logSvusage(std::ofstream &out)
{
    out << "数据中心当前具有服务器共：" << svnum << "台\n";            //输出服务器总数
    for (auto it = serverArray.begin(); it != serverArray.end(); it++) //输出每台服务器数目
    {
        it->usagePrint(out);
    }
}
bool Datalib::addSuccess(Vmbase *vmbase, int vmId)
{
    Vm *newVm = new Vm(vmbase); //按照用户需求添加一台虚拟机
    if (newVm->node == 0)       //如果新添加的虚拟机是单节点
    {
        for (auto it = serverArray.begin(); it != serverArray.end(); it++) //遍历数据库当中已有服务器
        {
            if (it->usedCpuA + newVm->core <= it->core / 2 && it->usedMemA + newVm->mem <= it->mem / 2) //当前服务器A节点可容纳虚拟机
            {
                addvm(newVm, vmId, &*it, 'A');
                return true;
            }
            if (it->usedCpuB + newVm->core <= it->core / 2 && it->usedMemB + newVm->mem <= it->mem / 2)
            {
                addvm(newVm, vmId, &*it, 'B');
                return true;
            }
        }
    }
    else
    {
        for (auto it = serverArray.begin(); it != serverArray.end(); it++)
        {
            if (it->usedCpuA + newVm->core / 2 <= it->core / 2 && it->usedMemA + newVm->mem / 2 <= it->mem / 2 && it->usedCpuB + newVm->core / 2 <= it->core / 2 && it->usedMemB + newVm->mem / 2 <= it->mem / 2)
            {
                addvm(newVm, vmId, &*it, 'X');
                return true;
            }
        }
    }
    return false;
}

double Datalib::matchDegree(Vm *vmbase, Server *sv, char nodetype)
{
    double matchdegree;
    if (vmbase->node == 0)
    {
        if (nodetype == 'A')
        {
            matchdegree = 1 - abs((2 * (sv->usedCpuA + vmbase->core) / sv->core) - (2 * (sv->usedMemA + vmbase->mem) / sv->mem));
        }
        if (nodetype == 'B')
        {
            matchdegree = 1 - abs((2 * (sv->usedCpuB + vmbase->core) / sv->core) - (2 * (sv->usedMemB + vmbase->mem) / sv->mem));
        }
    }
    else
    {
        matchdegree = 1 - abs(((sv->usedCpuA + sv->usedCpuB + vmbase->core) / sv->core) - ((sv->usedMemA + sv->usedMemB + vmbase->mem) / sv->mem));
    }
    return matchdegree;
}
bool Datalib::addSuccess_v2(Vmbase *vmbase, int vmId)
{
    Vm *newVm = new Vm(vmbase); //按照用户需求添加一台虚拟机
    Server *svTar;
    double matchdegreeMax = -1.;
    char nodetype;
    if (newVm->node == 0) //如果新添加的虚拟机是单节点
    {
        for (auto it = serverArray.begin(); it != serverArray.end(); it++) //遍历数据库当中已有服务器
        {
            if (it->usedCpuA + newVm->core <= it->core / 2 && it->usedMemA + newVm->mem <= it->mem / 2) //当前服务器A节点可容纳虚拟机
            {
                if (matchDegree(newVm, &*it, 'A') > matchdegreeMax)
                {
                    svTar = &*it;
                    nodetype = 'A';
                    matchdegreeMax = matchDegree(newVm, &*it, 'A');
                }
            }
            if (it->usedCpuB + newVm->core <= it->core / 2 && it->usedMemB + newVm->mem <= it->mem / 2)
            {
                if (matchDegree(newVm, &*it, 'B') > matchdegreeMax)
                {
                    svTar = &*it;
                    nodetype = 'B';
                    matchdegreeMax = matchDegree(newVm, &*it, 'B');
                }
            }
        }
        if (matchdegreeMax > -1.)
        {
            addvm(newVm, vmId, svTar, nodetype);
            return true;
        }
        else
            return false;
    }
    else
    {
        for (auto it = serverArray.begin(); it != serverArray.end(); it++)
        {
            if (it->usedCpuA + newVm->core / 2 <= it->core / 2 && it->usedMemA + newVm->mem / 2 <= it->mem / 2 && it->usedCpuB + newVm->core / 2 <= it->core / 2 && it->usedMemB + newVm->mem / 2 <= it->mem / 2)
            {
                if (matchDegree(newVm, &*it, 'X') > matchdegreeMax)
                {
                    svTar = &*it;
                    nodetype = 'X';
                    matchdegreeMax = matchDegree(newVm, &*it, 'X');
                }
            }
        }
        if (matchdegreeMax > -1.)
        {
            addvm(newVm, vmId, svTar, nodetype);
            return true;
        }
        else
            return false;
    }
    return false;
}

bool Datalib::addSuccess_v3(Vmbase *vmbase, int vmId)
{
    bool firstFlag = 0;
    Server *servercandidate = nullptr;
    char nodecandidate;
    Vm *newVm = new Vm(vmbase); //按照用户需求添加一台虚拟机
    bool acpudead, amemdead, bcpudead, bmemdead;
    bool acpualive, amemalive, bcpualive, bmemalive;
    if (newVm->node == 0) //如果新添加的虚拟机是单节点
    {
        for (auto it = serverArray.begin(); it != serverArray.end(); it++) //遍历数据库当中已有服务器
        {
            if (it->usedCpuA + newVm->core <= it->core / 2 && it->usedMemA + newVm->mem <= it->mem / 2) //当前服务器A节点可容纳虚拟机
            {
                if (firstFlag)
                {
                    servercandidate = &*it;
                    nodecandidate = 'A';
                }
                firstFlag = 0;
                acpudead = (it->core / 2 - newVm->core - it->usedCpuA) < CPUDEAD;
                amemdead = (it->mem / 2 - newVm->mem - it->usedMemA) < MEMDEAD;
                acpualive = (it->core / 2 - newVm->core - it->usedCpuA) > CPUALIVE;
                amemalive = (it->mem / 2 - newVm->mem - it->usedMemA) > MEMALIVE;
                if ((acpudead && amemalive) || (acpualive && amemdead))
                    continue;
                else
                {
                    addvm(newVm, vmId, &*it, 'A');
                    return true;
                }
            }
            if (it->usedCpuB + newVm->core <= it->core / 2 && it->usedMemB + newVm->mem <= it->mem / 2)
            {
                if (firstFlag)
                {
                    servercandidate = &*it;
                    nodecandidate = 'B';
                }
                firstFlag = 0;
                bcpudead = (it->core / 2 - newVm->core - it->usedCpuB) < CPUDEAD;
                bmemdead = (it->mem / 2 - newVm->mem - it->usedMemB) < MEMDEAD;
                bcpualive = (it->core / 2 - newVm->core - it->usedCpuB) > CPUALIVE;
                bmemalive = (it->mem / 2 - newVm->mem - it->usedMemB) > MEMALIVE;
                if ((bcpudead && bmemalive) || (bcpualive && bmemdead))
                    continue;
                else
                {
                    addvm(newVm, vmId, &*it, 'B');
                    return true;
                }
            }
        }
        if (servercandidate != nullptr)
        {
            addvm(newVm, vmId, servercandidate, nodecandidate);
            return true;
        }
    }
    else
    {
        for (auto it = serverArray.begin(); it != serverArray.end(); it++)
        {
            if (it->usedCpuA + newVm->core / 2 <= it->core / 2 && it->usedMemA + newVm->mem / 2 <= it->mem / 2 && it->usedCpuB + newVm->core / 2 <= it->core / 2 && it->usedMemB + newVm->mem / 2 <= it->mem / 2)
            {
                if (firstFlag)
                {
                    servercandidate = &*it;
                    nodecandidate = 'X';
                }
                firstFlag = 0;
                acpudead = (it->core / 2 - newVm->core / 2 - it->usedCpuA) < CPUDEAD;
                amemdead = (it->mem / 2 - newVm->mem / 2 - it->usedMemA) < MEMDEAD;
                acpualive = (it->core / 2 - newVm->core / 2 - it->usedCpuA) > CPUALIVE;
                amemalive = (it->mem / 2 - newVm->mem / 2 - it->usedMemA) > MEMALIVE;
                bcpudead = (it->core / 2 - newVm->core / 2 - it->usedCpuB) < CPUDEAD;
                bmemdead = (it->mem / 2 - newVm->mem / 2 - it->usedMemB) < MEMDEAD;
                bcpualive = (it->core / 2 - newVm->core / 2 - it->usedCpuB) > CPUALIVE;
                bmemalive = (it->mem / 2 - newVm->mem / 2 - it->usedMemB) > MEMALIVE;
                if ((acpudead || amemdead || bcpudead || bmemdead) && (acpualive || bcpualive || amemalive || bmemalive))
                    continue;
                else
                {
                    addvm(newVm, vmId, &*it, 'X');
                    return true;
                }
            }
        }
        if (servercandidate != nullptr)
        {
            addvm(newVm, vmId, servercandidate, nodecandidate);
            return true;
        }
    }
    return false;
}
//执行用户请求的add操作
//遍历已有的服务器
//返回是否有服务器可以容纳请求
bool Datalib::addExistServerSuccess(Req *req)
{
    //根据请求创建一台新虚拟机，并赋予其id
    Vm *newVm = new Vm(vmNameArray[req->vmName]);
    int vmId = req->vmId;
    //遍历已存在的服务器，选出虚拟机要存放的服务器地址和节点
    //尽量避免虚拟机的添加造成服务器出现死空间
    Server *servercandidate = nullptr; //出现死空间的服务器用于备选
    char nodecandidate;
    for (auto it = serverArray.begin(); it != serverArray.end(); it++)
    {
        char nodeToAdd = abletoAdd(it, newVm);
        if (nodeToAdd && !caseDeadspace(it, nodeToAdd, newVm))
        {

            req->ack = addvmNew(newVm, vmId, &*it, nodeToAdd);
            return true;
        }
        else if (nodeToAdd && caseDeadspace(it, nodeToAdd, newVm))
        {
            servercandidate = &*it;
            nodecandidate = nodeToAdd;
        }
    }
    if (servercandidate != nullptr)
    {
        req->ack = addvmNew(newVm, vmId, servercandidate, nodecandidate);
        return true;
    }
    else
        return false;
}
char Datalib::abletoAdd(std::list<Server>::iterator it, Vm *newVm)
{
    if (newVm->node == 0)
    {
        if (it->usedCpuA + newVm->core <= it->core / 2 && it->usedMemA + newVm->mem <= it->mem / 2)
            return 'A';
        else if (it->usedCpuB + newVm->core <= it->core / 2 && it->usedMemB + newVm->mem <= it->mem / 2)
            return 'B';
        else
            return 0;
    }
    else
    {
        if (it->usedCpuA + newVm->core / 2 <= it->core / 2 &&
            it->usedMemA + newVm->mem / 2 <= it->mem / 2 &&
            it->usedCpuB + newVm->core / 2 <= it->core / 2 &&
            it->usedMemB + newVm->mem / 2 <= it->mem / 2)
            return 'X';
        else
            return 0;
    }
}
bool Datalib::caseDeadspace(std::list<Server>::iterator it, char nodeToAdd, Vm *newVm)
{
    bool acpudead, amemdead, bcpudead, bmemdead;
    bool acpualive, amemalive, bcpualive, bmemalive;
    switch (nodeToAdd)
    {
    case 'A':
    {
        acpudead = (it->core / 2 - newVm->core - it->usedCpuA) < CPUDEAD;
        amemdead = (it->mem / 2 - newVm->mem - it->usedMemA) < MEMDEAD;
        acpualive = (it->core / 2 - newVm->core - it->usedCpuA) > CPUALIVE;
        amemalive = (it->mem / 2 - newVm->mem - it->usedMemA) > MEMALIVE;
        if ((acpudead && amemalive) || (acpualive && amemdead))
            return true;
        break;
    }
    case 'B':
    {
        bcpudead = (it->core / 2 - newVm->core - it->usedCpuB) < CPUDEAD;
        bmemdead = (it->mem / 2 - newVm->mem - it->usedMemB) < MEMDEAD;
        bcpualive = (it->core / 2 - newVm->core - it->usedCpuB) > CPUALIVE;
        bmemalive = (it->mem / 2 - newVm->mem - it->usedMemB) > MEMALIVE;
        if ((bcpudead && bmemalive) || (bcpualive && bmemdead))
            return true;
        break;
    }
    case 'X':
    {
        acpudead = (it->core / 2 - newVm->core / 2 - it->usedCpuA) < CPUDEAD;
        amemdead = (it->mem / 2 - newVm->mem / 2 - it->usedMemA) < MEMDEAD;
        acpualive = (it->core / 2 - newVm->core / 2 - it->usedCpuA) > CPUALIVE;
        amemalive = (it->mem / 2 - newVm->mem / 2 - it->usedMemA) > MEMALIVE;
        bcpudead = (it->core / 2 - newVm->core / 2 - it->usedCpuB) < CPUDEAD;
        bmemdead = (it->mem / 2 - newVm->mem / 2 - it->usedMemB) < MEMDEAD;
        bcpualive = (it->core / 2 - newVm->core / 2 - it->usedCpuB) > CPUALIVE;
        bmemalive = (it->mem / 2 - newVm->mem / 2 - it->usedMemB) > MEMALIVE;
        if ((acpudead || amemdead || bcpudead || bmemdead) && (acpualive || bcpualive || amemalive || bmemalive))
            return true;
        break;
    }
    }
    return false;
}
bool Datalib::addSvTodaySuccess(Req *req)
{
    Vmbase *vmbase = vmNameArray[req->vmName];
    int vmId = req->vmId;
    bool firstFlag = 0;
    Server *servercandidate = nullptr;
    char nodecandidate;
    Vm *newVm = new Vm(vmbase); //按照用户需求添加一台虚拟机
    bool acpudead, amemdead, bcpudead, bmemdead;
    bool acpualive, amemalive, bcpualive, bmemalive;
    if (newVm->node == 0) //如果新添加的虚拟机是单节点
    {
        for (auto it = newServerToday.begin(); it != newServerToday.end(); it++) //遍历数据库当中已有服务器
        {
            if (it->usedCpuA + newVm->core <= it->core / 2 && it->usedMemA + newVm->mem <= it->mem / 2) //当前服务器A节点可容纳虚拟机
            {
                if (firstFlag)
                {
                    servercandidate = &*it;
                    nodecandidate = 'A';
                }
                firstFlag = 0;
                acpudead = (it->core / 2 - newVm->core - it->usedCpuA) < CPUDEAD;
                amemdead = (it->mem / 2 - newVm->mem - it->usedMemA) < MEMDEAD;
                acpualive = (it->core / 2 - newVm->core - it->usedCpuA) > CPUALIVE;
                amemalive = (it->mem / 2 - newVm->mem - it->usedMemA) > MEMALIVE;
                if ((acpudead && amemalive) || (acpualive && amemdead))
                    continue;
                else
                {
                    req->ack = addvmNew(newVm, vmId, &*it, 'A');
                    return true;
                }
            }
            if (it->usedCpuB + newVm->core <= it->core / 2 && it->usedMemB + newVm->mem <= it->mem / 2)
            {
                if (firstFlag)
                {
                    servercandidate = &*it;
                    nodecandidate = 'B';
                }
                firstFlag = 0;
                bcpudead = (it->core / 2 - newVm->core - it->usedCpuB) < CPUDEAD;
                bmemdead = (it->mem / 2 - newVm->mem - it->usedMemB) < MEMDEAD;
                bcpualive = (it->core / 2 - newVm->core - it->usedCpuB) > CPUALIVE;
                bmemalive = (it->mem / 2 - newVm->mem - it->usedMemB) > MEMALIVE;
                if ((bcpudead && bmemalive) || (bcpualive && bmemdead))
                    continue;
                else
                {
                    req->ack = addvmNew(newVm, vmId, &*it, 'B');
                    return true;
                }
            }
        }
        if (servercandidate != nullptr)
        {
            req->ack = addvmNew(newVm, vmId, servercandidate, nodecandidate);
            return true;
        }
    }
    else
    {
        for (auto it = newServerToday.begin(); it != newServerToday.end(); it++)
        {
            if (it->usedCpuA + newVm->core / 2 <= it->core / 2 && it->usedMemA + newVm->mem / 2 <= it->mem / 2 && it->usedCpuB + newVm->core / 2 <= it->core / 2 && it->usedMemB + newVm->mem / 2 <= it->mem / 2)
            {
                if (firstFlag)
                {
                    servercandidate = &*it;
                    nodecandidate = 'X';
                }
                firstFlag = 0;
                acpudead = (it->core / 2 - newVm->core / 2 - it->usedCpuA) < CPUDEAD;
                amemdead = (it->mem / 2 - newVm->mem / 2 - it->usedMemA) < MEMDEAD;
                acpualive = (it->core / 2 - newVm->core / 2 - it->usedCpuA) > CPUALIVE;
                amemalive = (it->mem / 2 - newVm->mem / 2 - it->usedMemA) > MEMALIVE;
                bcpudead = (it->core / 2 - newVm->core / 2 - it->usedCpuB) < CPUDEAD;
                bmemdead = (it->mem / 2 - newVm->mem / 2 - it->usedMemB) < MEMDEAD;
                bcpualive = (it->core / 2 - newVm->core / 2 - it->usedCpuB) > CPUALIVE;
                bmemalive = (it->mem / 2 - newVm->mem / 2 - it->usedMemB) > MEMALIVE;
                if ((acpudead || amemdead || bcpudead || bmemdead) && (acpualive || bcpualive || amemalive || bmemalive))
                    continue;
                else
                {
                    req->ack = addvmNew(newVm, vmId, &*it, 'X');
                    return true;
                }
            }
        }
        if (servercandidate != nullptr)
        {
            req->ack = addvmNew(newVm, vmId, servercandidate, nodecandidate);

            return true;
        }
    }
    return false;
}

bool Datalib::del(int vmId)
{
    --vmnum;
    Vm *vmDel = vmMap[vmId];
    if (vmDel == nullptr) //没有找到当前要请求的虚拟机
        return false;
    Server *sv = vmDel->svm.first;
    char nodetype = vmDel->svm.second;
    sv->cpurate -= double(vmDel->core) / sv->core;
    sv->memrate -= double(vmDel->mem) / sv->mem;
    usedCpu -= vmDel->core;
    usedMem -= vmDel->mem;
    if (nodetype == 'X') //如果此vm是双节点
    {
        sv->usedCpuA -= vmDel->core / 2;
        sv->usedMemA -= vmDel->mem / 2;
        sv->usedCpuB -= vmDel->core / 2;
        sv->usedMemB -= vmDel->mem / 2;
    }
    if (nodetype == 'A')
    {
        sv->usedCpuA -= vmDel->core;
        sv->usedMemA -= vmDel->mem;
    }
    if (nodetype == 'B')
    {
        sv->usedCpuB -= vmDel->core;
        sv->usedMemB -= vmDel->mem;
    }

    for (auto itv = sv->vms.begin(); itv != sv->vms.end(); itv++) //将此vm在数据库服务器保存的vms中删除
    {
        if ((*itv).first == vmId)
        {
            sv->vms.erase(itv);
            break;
        }
    }
    for (auto vm = sv->vmMembers.begin(); vm != sv->vmMembers.end(); vm++)
    {
        if ((*vm)->vmId == vmDel->vmId) //两个地址不能比较相等
        {
            sv->vmMembers.erase(vm);
            break;
        }
    }
    return true;
}

void Datalib::addvm(Vm *newVm, int vmId, Server *sv, const char nodetype) //添加虚拟机到服务器
{
    sv->TurnOnflag = 1; //test
    vmnum++;
    newVm->vmId = vmId;
    newVm->svm = {sv, nodetype}; //定义该虚拟机对应的服务器
    sv->vms.push_back({vmId, nodetype});
    sv->vmMembers.insert(newVm);
    vmArray.push_back(*newVm); //添加当前虚拟机到vmArray
    //vmMap.insert({vmid, &vmArray.back()})
    vmMap.insert({vmId, newVm}); //将该虚拟机存在数据库字典中
    usedCpu += newVm->core;
    usedMem += newVm->mem;
    sv->cpurate += double(newVm->core) / sv->core;
    sv->memrate += double(newVm->mem) / sv->mem;
    if (nodetype == 'X') //如果虚拟机是双节点
    {
        sv->usedCpuA += newVm->core / 2;
        sv->usedMemA += newVm->mem / 2;
        sv->usedCpuB += newVm->core / 2;
        sv->usedMemB += newVm->mem / 2;
        logaddMessage.push_back({sv, 'X'});
    }
    else
    {
        if (nodetype == 'A')
        {
            sv->usedCpuA += newVm->core;
            sv->usedMemA += newVm->mem;
            logaddMessage.push_back({sv, 'A'});
        }
        else
        {
            sv->usedCpuB += newVm->core;
            sv->usedMemB += newVm->mem;
            logaddMessage.push_back({sv, 'B'});
        }
    }
#if 1
    if (sv->usedCpuA > sv->core / 2 || sv->usedCpuB > sv->core / 2 || sv->usedMemA > sv->mem / 2 || sv->usedMemB > sv->mem / 2)
        cout << "ERROR: Resource overflow" << '\n';
#endif
}

void Datalib::purchasebyuser(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int remainDays)
{
    ++svnum;
    totCpu += base->core;
    totMem += base->mem;
    Vm *newVm = new Vm(base); //按照用户需求添加一台虚拟机
    double minCost = INF;
    double costTot;
    Serverbase *svp = serverLib;                                       //候选服务器
    double pac = 0.001, pam = 0.001;                                   //用来统计现存服务器中总的cpu和mem的使用数
    for (auto it = serverArray.begin(); it != serverArray.end(); it++) //遍历数据库当中已有服务器计算当前使用的总cpu和内存
    {
        pac += (it->usedCpuA + it->usedCpuB);
        pam += (it->usedMemA + it->usedMemB);
    }
    double rate = pac / pam;
    int memUable = 0, cpuUable = 0;
    if (newVm->node == 1) //请求的虚拟机为双节点
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++) //遍历选出服务器
        {
            svt = serverLib + i;
            if (svt->core >= newVm->core && svt->mem >= newVm->mem)
            {
                //计算成本
                cpuUable = std::min(svt->core, int(svt->mem * rate));
                memUable = std::min(svt->mem, int(svt->core / rate));
                costTot = (svt->hdcost + svt->elecost * remainDays) / (cpuUable + memUable);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverTar->rank = svnum;
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'X'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    else
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++)
        {
            svt = serverLib + i;
            if (svt->core / 2 >= newVm->core && svt->mem / 2 >= newVm->mem)
            {
                //计算成本
                cpuUable = std::min(svt->core, int(svt->mem * rate));
                memUable = std::min(svt->mem, int(svt->core / rate));
                costTot = (svt->hdcost + svt->elecost * remainDays) / (cpuUable + memUable);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverTar->rank = svnum;
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'A'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    totCpu += svp->core;
    totMem += svp->mem;
    ++purNum;
}

void Datalib::purchasebyUsenum(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int remainDays)
{
    ++svnum;
    totCpu += base->core;
    totMem += base->mem;
    Vm *newVm = new Vm(base); //按照用户需求添加一台虚拟机
    double minCost = INF;
    double costTot;
    Serverbase *svp = serverLib;                                       //候选服务器
    double pac = 0., pam = 0.;                                         //用来统计现存服务器中总的cpu和mem的使用率
    double pc, pm;                                                     //计算服务器成本的时候的两个系数
    for (auto it = serverArray.begin(); it != serverArray.end(); it++) //遍历数据库当中已有服务器
    {
        pac += (it->usedCpuA + it->usedCpuB);
        pam += (it->usedMemA + it->usedMemB);
    }
    double optiPara;
    optiPara = double(pac) / pam; //采用动态参数并不能减小成本
    //optiPara = 1.;
    pc = optiPara / (optiPara + 1);
    pm = 1 - pc;
    if (newVm->node == 1) //请求的虚拟机为双节点
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++) //遍历选出服务器
        {
            svt = serverLib + i;
            if (svt->core > newVm->core && svt->mem > newVm->mem)
            {
                costTot = pc * ((svt->hdcost + svt->elecost * remainDays) / svt->core) + pm * ((svt->hdcost + svt->elecost * remainDays) / svt->mem);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverTar->rank = purNum;
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'X'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    else
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++)
        {
            svt = serverLib + i;
            if (svt->core / 2 > newVm->core && svt->mem / 2 > newVm->mem)
            {
                costTot = pc * ((svt->hdcost + svt->elecost * remainDays) / svt->core) + pm * ((svt->hdcost + svt->elecost * remainDays) / svt->mem);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverTar->rank = purNum;
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'A'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    totCpu += svp->core;
    totMem += svp->mem;
    ++purNum;
}

void Datalib::logSvusenum(std::ofstream &out)
{
    int usecpu = 0, usemem = 0, totcpu = 0, totmem = 0;
    for (auto it = serverArray.begin(); it != serverArray.end(); it++) //输出每台服务器数目
    {
        usecpu += it->usedCpuA + it->usedCpuB;
        usemem += it->usedMemA + it->usedMemB;
        totcpu += it->core;
        totmem += it->mem;
    }
    if (usedCpu == usecpu && usedMem == usemem)
        out << "usecpu," << usecpu << ",usemem," << usemem << ",totcpu," << totcpu << ",totmem," << totmem << '\n';
    else
        std::cerr << "WRONG Calcu of usedcpu ";
}

void Datalib::migrate()
{
    //对当前服务器遍历，判断每一台服务器是否需要迁移优化
    for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++)
    {
        if (migNum >= vmnumToday / 34)
        {
            return;
        }
        bool acpudead, amemdead, bcpudead, bmemdead;
        acpudead = (sv->core / 2 - sv->usedCpuA) < CPUDEAD;
        amemdead = (sv->mem / 2 - sv->usedMemA) < MEMDEAD;
        bcpudead = (sv->core / 2 - sv->usedCpuB) < CPUDEAD;
        bmemdead = (sv->core / 2 - sv->usedMemB) < MEMDEAD;
        bool acpualive, amemalive, bcpualive, bmemalive;
        acpualive = (sv->core / 2 - sv->usedCpuA) > CPUALIVE;
        amemalive = (sv->mem / 2 - sv->usedMemA) > MEMALIVE;
        bcpualive = (sv->core / 2 - sv->usedCpuB) > CPUALIVE;
        bmemalive = (sv->mem / 2 - sv->usedMemA) > MEMALIVE;
        Vm *vmmig = nullptr;
        if (acpudead && amemalive) //对A节店内cpu占用较多虚拟机进行迁移
        {
            for (auto vm = sv->vms.begin(); vm != sv->vms.end(); vm++) //对节点和虚拟机进行遍历
            {
                if (((vm->second == 'A') && ((vmMap[vm->first]->core - vmMap[vm->first]->mem) > CPUALIVE)) ||
                    ((vm->second == 'X') && ((vmMap[vm->first]->core - vmMap[vm->first]->mem) > 2 * CPUALIVE)))
                {
                    vmmig = vmMap[vm->first];
                    break;
                }
            }
        }
        if (acpualive && amemdead) //对A节店内mem占用较多虚拟机进行迁移
        {
            for (auto vm = sv->vms.begin(); vm != sv->vms.end(); vm++) //对节点和虚拟机进行遍历
            {
                if (((vm->second == 'A') && ((vmMap[vm->first]->mem - vmMap[vm->first]->core) > MEMALIVE)) ||
                    ((vm->second == 'X') && ((vmMap[vm->first]->mem - vmMap[vm->first]->core) > 2 * MEMALIVE)))
                {
                    vmmig = vmMap[vm->first];
                    break;
                }
            }
        }
        if (bcpudead && bmemalive) //对B节店内cpu占用较多虚拟机进行迁移
        {
            for (auto vm = sv->vms.begin(); vm != sv->vms.end(); vm++) //对节点和虚拟机进行遍历
            {
                if (((vm->second == 'B') && ((vmMap[vm->first]->core - vmMap[vm->first]->mem) > CPUALIVE)) ||
                    ((vm->second == 'X') && ((vmMap[vm->first]->core - vmMap[vm->first]->mem) > 2 * CPUALIVE)))
                {
                    vmmig = vmMap[vm->first];
                    break;
                }
            }
        }
        if (bcpualive && bmemdead) //对B节店内mem占用较多虚拟机进行迁移
        {
            for (auto vm = sv->vms.begin(); vm != sv->vms.end(); vm++) //对节点和虚拟机进行遍历
            {
                if (((vm->second == 'B') && ((vmMap[vm->first]->mem - vmMap[vm->first]->core) > MEMALIVE)) ||
                    ((vm->second == 'X') && ((vmMap[vm->first]->mem - vmMap[vm->first]->core) > 2 * MEMALIVE)))
                {
                    vmmig = vmMap[vm->first];
                    break;
                }
            }
        }
        if (vmmig != nullptr)
        {
            for (auto it = serverArray.begin(); it != serverArray.end(); it++) //得到迁移虚拟机vmmig的目标迁移服务器
            {
                if (vmmig->node == 1)
                {
                    if (it->usedCpuA + vmmig->core / 2 <= it->core / 2 && it->usedMemA + vmmig->mem / 2 <= it->mem / 2 &&
                        it->usedCpuB + vmmig->core / 2 <= it->core / 2 && it->usedMemB + vmmig->mem / 2 <= it->mem / 2) //当前服务器可容纳该双节点虚拟机
                    {
                        if (&*it != vmmig->svm.first)
                        {
                            migvm2sv(vmmig, &*it, 'X');
                            break;
                        }
                    }
                }
                else
                {
                    if (it->usedCpuA + vmmig->core <= it->core / 2 && it->usedMemA + vmmig->mem <= it->mem / 2) //当前服务器A节点可容纳虚拟机
                    {
                        if (&*it != vmmig->svm.first || vmmig->svm.second != 'A')
                        {
                            migvm2sv(vmmig, &*it, 'A');
                            break;
                        }
                    }
                    if (it->usedCpuB + vmmig->core <= it->core / 2 && it->usedMemB + vmmig->mem <= it->mem / 2)
                    {
                        if (&*it != vmmig->svm.first || vmmig->svm.second != 'B')
                        {
                            migvm2sv(vmmig, &*it, 'B');
                            break;
                        }
                    }
                }
            }
        }
    }
}

void Datalib::purchase_MQ(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int day) //数据库购买服务器
{
    ++svnum;
    totCpu += base->core;
    totMem += base->mem;
    Vm *newVm = new Vm(base); //按照用户需求添加一台虚拟机
    double minCost = INF;
    double costTot;
    Serverbase *svp = serverLib;       //候选服务器
    double pac = 0.0001, pam = 0.0001; //用来统计现存服务器中总的cpu和mem的使用率
    double pc, pm;
    int leng = 0;                                                      //计算服务器成本的时候的两个系数
    for (auto it = serverArray.begin(); it != serverArray.end(); it++) //遍历数据库当中已有服务器
    {
        ++leng;
        pac += it->paycore;
        pam += it->paymem;
    }
    pc = pac / leng;
    pm = pam / leng;
    if (pac > pam)
    {
        pc = 2.5;
        pm = 1.0;
    }
    else
    {
        pm = 1.3;
        pc = 1.0;
    }
    if (newVm->node == 1) //请求的虚拟机为双节点
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++) //遍历选出服务器
        {
            svt = serverLib + i;
            if (svt->core > newVm->core && svt->mem > newVm->mem)
            {
                costTot = (svt->hdcost + svt->elecost * day) / (svt->core * pc + svt->mem * pm);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'X'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    else
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++)
        {
            svt = serverLib + i;
            if (svt->core / 2 > newVm->core && svt->mem / 2 > newVm->mem)
            {
                costTot = (svt->hdcost + svt->elecost * day) / (svt->core * pc + svt->mem * pm);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'A'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    ++purNum;
}

void Datalib::purchase(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int day) //数据库购买服务器
{
    ++svnum;
    totCpu += base->core;
    totMem += base->mem;
    Vm *newVm = new Vm(base); //按照用户需求添加一台虚拟机
    double minCost = INF;
    double costTot;
    Serverbase *svp = serverLib;       //候选服务器
    double pac = 0.0001, pam = 0.0001; //用来统计现存服务器中总的cpu和mem的使用率
    double pc, pm;
    int leng = 0;                                                      //计算服务器成本的时候的两个系数
    for (auto it = serverArray.begin(); it != serverArray.end(); it++) //遍历数据库当中已有服务器
    {
        ++leng;
        pac += it->paycore;
        pam += it->paymem;
    }
    pc = pac / leng;
    pm = pam / leng;
    // if (pac > pam)
    // {
    //     pc = 2.5;
    //     pm = 1.0;
    // }
    // else
    // {
    //     pm = 1.3;
    //     pc = 1.0;
    // }
    if (newVm->node == 1) //请求的虚拟机为双节点
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++) //遍历选出服务器
        {
            svt = serverLib + i;
            if (svt->core > newVm->core && svt->mem > newVm->mem)
            {
                costTot = (svt->hdcost + svt->elecost * day) / (svt->core * pc + svt->mem * pm);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'X'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    else
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++)
        {
            svt = serverLib + i;
            if (svt->core / 2 > newVm->core && svt->mem / 2 > newVm->mem)
            {
                costTot = (svt->hdcost + svt->elecost * day) / (svt->core * pc + svt->mem * pm);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'A'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    ++purNum;
}

bool Datalib::add_migsuccess(Vmbase *vmbase, int vmId, int migdepth)
{
    if (addSuccess_v3(vmbase, vmId))
        return true;
    else if (!addSuccess_v3(vmbase, vmId) && (migNum + migdepth) <= floor(0.005 * vmnumToday)) //通过迁移migdepth步实现将当前虚拟机放入datalib
    {
        //遍历所有服务器，检查是否可以通过迁移的方式将当前虚拟机填充进去
        for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++)
        {
            Vm *vmcandi = nullptr; //目标迁移虚拟机
            char nodecandi;        //目标add节点
            if (vmbase->node == 0) //当前请求为单节点虚拟机
            {
                //遍历当前服务器中的虚拟机，检查是否可以与当前的虚拟机请求交换
                for (auto vm = sv->vms.begin(); vm != sv->vms.end(); vm++) //当前虚拟机：vmMap[vm->first]
                {
                    //判断当前的虚拟机是否可以与目标虚拟机交换
                    if (vm->second == 'A')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA - vmMap[vm->first]->core + vmbase->core) &&
                            (sv->mem / 2 >= sv->usedMemA - vmMap[vm->first]->mem + vmbase->mem) &&
                            ((vmbase->core + vmbase->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmcandi == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmcandi->core + vmcandi->mem)) //比备选服务器规模更小
                            {
                                vmcandi = vmMap[vm->first];
                                nodecandi = 'A';
                            }
                        }
                    }
                    if (vm->second == 'B')
                    {
                        if ((sv->core / 2 >= sv->usedCpuB - vmMap[vm->first]->core + vmbase->core) &&
                            (sv->mem / 2 >= sv->usedMemB - vmMap[vm->first]->mem + vmbase->mem) &&
                            ((vmbase->core + vmbase->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmcandi == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmcandi->core + vmcandi->mem)) //比备选服务器规模更小
                            {
                                vmcandi = vmMap[vm->first];
                                nodecandi = 'B';
                            }
                        }
                    }
                    if (vm->second == 'X')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA - vmMap[vm->first]->core / 2 + vmbase->core) &&
                            (sv->mem / 2 >= sv->usedMemA - vmMap[vm->first]->mem / 2 + vmbase->mem) &&
                            ((vmbase->core + vmbase->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmcandi == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmcandi->core + vmcandi->mem)) //比备选服务器规模更小
                            {
                                vmcandi = vmMap[vm->first];
                                nodecandi = 'A';
                            }
                        }
                        else if ((sv->core / 2 >= sv->usedCpuB - vmMap[vm->first]->core / 2 + vmbase->core) &&
                                 (sv->mem / 2 >= sv->usedMemB - vmMap[vm->first]->mem / 2 + vmbase->mem) &&
                                 ((vmbase->core + vmbase->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmcandi == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmcandi->core + vmcandi->mem)) //比备选服务器规模更小
                            {
                                vmcandi = vmMap[vm->first];
                                nodecandi = 'B';
                            }
                        }
                    }
                }
            } //end of node == 0
            else if (vmbase->node == 1)
            {
                //遍历当前服务器中的虚拟机，检查是否可以与当前的虚拟机请求交换
                for (auto vm = sv->vms.begin(); vm != sv->vms.end(); vm++) //当前虚拟机：vmMap[vm->first]
                {
                    //判断当前的虚拟机是否可以与目标虚拟机交换
                    if (vm->second == 'A')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA - vmMap[vm->first]->core + vmbase->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemA - vmMap[vm->first]->mem + vmbase->mem / 2) &&
                            (sv->core / 2 >= sv->usedCpuB + vmbase->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemB + vmbase->mem / 2) &&
                            ((vmbase->core + vmbase->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmcandi == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmcandi->core + vmcandi->mem)) //比备选服务器规模更小
                            {
                                vmcandi = vmMap[vm->first];
                                nodecandi = 'X';
                            }
                        }
                    }
                    if (vm->second == 'B')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA + vmbase->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemA + vmbase->mem / 2) &&
                            (sv->core / 2 >= sv->usedCpuB - vmMap[vm->first]->core + vmbase->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemB - vmMap[vm->first]->mem + vmbase->mem / 2) &&
                            ((vmbase->core + vmbase->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmcandi == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmcandi->core + vmcandi->mem)) //比备选服务器规模更小
                            {
                                vmcandi = vmMap[vm->first];
                                nodecandi = 'X';
                            }
                        }
                    }
                    if (vm->second == 'X')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA - vmMap[vm->first]->core / 2 + vmbase->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemA - vmMap[vm->first]->mem / 2 + vmbase->mem / 2) &&
                            (sv->core / 2 >= sv->usedCpuB - vmMap[vm->first]->core / 2 + vmbase->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemB - vmMap[vm->first]->mem / 2 + vmbase->mem / 2) &&
                            ((vmbase->core + vmbase->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem)) //默认填A节点
                        {
                            if ((vmcandi == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmcandi->core + vmcandi->mem)) //比备选服务器规模更小
                            {
                                vmcandi = vmMap[vm->first];
                                nodecandi = 'X';
                            }
                        }
                    }
                }
            } //node == 1
            bool vmcandiToday;
            if (vmcandi != nullptr)
            {
                for (size_t vmcnt = 0; vmcnt < vmArrayToday.size(); vmcnt++)
                {
                    if (vmcandi->vmId == vmArrayToday[vmcnt]->vmId)
                        vmcandiToday = 1;
                }
                if (!vmcandiToday) //执行迁移算法成功，就把当前虚拟机add到当前服务器对应节点
                {
                    if (migAvm(vmcandi)) //将vmcandi迁移到其他服务器
                    {
                        Vm *newvm = new Vm(vmbase);
                        addvm(newvm, vmId, &*sv, nodecandi);
                        return true;
                    }
                }
            }
        } //遍历服务器
    }
    return false;
}
bool Datalib::migAvm(Vm *firstvm, int depth) //depth为迁移深度
{
    std::stack<Server *> Svstack;
    std::stack<char> Svnode;
    std::stack<Vm *> Vmstack;
    Vmstack.push(firstvm);
    Vm *vmcur = firstvm;
    while (depth--)
    {
        Server *svnext = nullptr;
        char nodenext;
        Vm *vmnext = nullptr;
        /*求vmnext svnext nodenext*/
        for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++) //遍历服务器
        {
            if (vmcur->node == 0) //当前请求为单节点虚拟机
            {
                //先判断能否放入该虚拟机
                if (sv->usedCpuA + vmcur->core <= sv->core / 2 && sv->usedMemA + vmcur->mem <= sv->mem / 2) //当前服务器A节点可容纳虚拟机
                {
                    svnext = &*sv;
                    nodenext = 'A';
                    break;
                }
                if (sv->usedCpuB + vmcur->core <= sv->core / 2 && sv->usedMemB + vmcur->mem <= sv->mem / 2)
                {
                    svnext = &*sv;
                    nodenext = 'B';
                    break;
                }
                //遍历当前服务器中的虚拟机，检查是否可以与当前的虚拟机请求交换
                for (auto vm = sv->vms.begin(); vm != sv->vms.end(); vm++) //当前虚拟机：vmMap[vm->first]
                {
                    //判断当前的虚拟机是否可以与目标虚拟机交换
                    if (vm->second == 'A')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA - vmMap[vm->first]->core + vmcur->core) &&
                            (sv->mem / 2 >= sv->usedMemA - vmMap[vm->first]->mem + vmcur->mem) &&
                            ((vmcur->core + vmcur->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmnext == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmnext->core + vmnext->mem)) //比备选服务器规模更小
                            {
                                vmnext = vmMap[vm->first];
                                nodenext = 'A';
                                svnext = &*sv;
                            }
                        }
                    }
                    if (vm->second == 'B')
                    {
                        if ((sv->core / 2 >= sv->usedCpuB - vmMap[vm->first]->core + vmcur->core) &&
                            (sv->mem / 2 >= sv->usedMemB - vmMap[vm->first]->mem + vmcur->mem) &&
                            ((vmcur->core + vmcur->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmnext == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmnext->core + vmnext->mem)) //比备选服务器规模更小
                            {
                                vmnext = vmMap[vm->first];
                                nodenext = 'B';
                                svnext = &*sv;
                            }
                        }
                    }
                    if (vm->second == 'X')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA - vmMap[vm->first]->core / 2 + vmcur->core) &&
                            (sv->mem / 2 >= sv->usedMemB - vmMap[vm->first]->mem / 2 + vmcur->mem) &&
                            ((vmcur->core + vmcur->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem)) //默认填A节点
                        {
                            if ((vmnext == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmnext->core + vmnext->mem)) //比备选服务器规模更小
                            {
                                vmnext = vmMap[vm->first];
                                nodenext = 'A';
                                svnext = &*sv;
                            }
                        }
                    }
                }
            } //end of node == 0
            else if (vmcur->node == 1)
            {
                if (sv->usedCpuA + vmcur->core / 2 <= sv->core / 2 && sv->usedMemA + vmcur->mem / 2 <= sv->mem / 2 &&
                    sv->usedCpuB + vmcur->core / 2 <= sv->core / 2 && sv->usedMemB + vmcur->mem / 2 <= sv->mem / 2) //当前服务器可容纳虚拟机
                {
                    svnext = &*sv;
                    nodenext = 'X';
                    break;
                }
                //遍历当前服务器中的虚拟机，检查是否可以与当前的虚拟机请求交换
                for (auto vm = sv->vms.begin(); vm != sv->vms.end(); vm++) //当前虚拟机：vmMap[vm->first]
                {
                    //判断当前的虚拟机是否可以与目标虚拟机交换
                    if (vm->second == 'A')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA - vmMap[vm->first]->core + vmcur->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemA - vmMap[vm->first]->mem + vmcur->mem / 2) &&
                            (sv->core / 2 >= sv->usedCpuB + vmcur->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemB + vmcur->mem / 2) &&
                            ((vmcur->core + vmcur->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmnext == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmnext->core + vmnext->mem)) //比备选服务器规模更小
                            {
                                vmnext = vmMap[vm->first];
                                nodenext = 'X';
                                svnext = &*sv;
                            }
                        }
                    }
                    if (vm->second == 'B')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA + vmcur->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemA + vmcur->mem / 2) &&
                            (sv->core / 2 >= sv->usedCpuB - vmMap[vm->first]->core + vmcur->core / 2) &&
                            (sv->mem / 2 >= sv->usedMemB - vmMap[vm->first]->core + vmcur->mem / 2) &&
                            ((vmcur->core + vmcur->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem))
                        {
                            if ((vmnext == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmnext->core + vmnext->mem)) //比备选服务器规模更小
                            {
                                vmnext = vmMap[vm->first];
                                nodenext = 'X';
                                svnext = &*sv;
                            }
                        }
                    }
                    if (vm->second == 'X')
                    {
                        if ((sv->core / 2 >= sv->usedCpuA - vmMap[vm->first]->core / 2 + vmcur->core) &&
                            (sv->mem / 2 >= sv->usedMemA - vmMap[vm->first]->mem / 2 + vmcur->mem) &&
                            (sv->core / 2 >= sv->usedCpuB - vmMap[vm->first]->core / 2 + vmcur->core) &&
                            (sv->mem / 2 >= sv->usedMemB - vmMap[vm->first]->mem / 2 + vmcur->mem) &&
                            ((vmcur->core + vmcur->mem) > vmMap[vm->first]->core + vmMap[vm->first]->mem)) //默认填A节点
                        {
                            if ((vmnext == nullptr) || (vmMap[vm->first]->core + vmMap[vm->first]->mem < vmnext->core + vmnext->mem)) //比备选服务器规模更小
                            {
                                vmnext = vmMap[vm->first];
                                nodenext = 'X';
                                svnext = &*sv;
                            }
                        }
                    }
                }
            }
        } //遍历服务器以及虚拟机
        if (svnext == nullptr)
            return false;
        else
        {
            Svstack.push(svnext);
            Svnode.push(nodenext);
            if (vmnext == nullptr)
            {
                vmcur = vmnext;
                break;
            }
            else
            {
                Vmstack.push(vmnext);
                vmcur = vmnext;
            }
        }
    }
    if (vmcur != nullptr)
        return false;
    else
    {
        while (!Svstack.empty()) //将虚拟机依次迁移到服务器
        {
            Server *svmigtemp = Svstack.top();
            Vm *vmmigtemp = Vmstack.top();
            char nodemigtemp = Svnode.top();
            migvm2sv(vmmigtemp, svmigtemp, nodemigtemp);
            Svstack.pop();
            Vmstack.pop();
            Svnode.pop();
        }
        return true;
    }
}
bool Datalib::migAvm(Vm *vmcur)
{
    Server *svnext = nullptr;
    char nodenext;
    for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++) //遍历服务器
    {
        if (vmcur->node == 0) //当前请求为单节点虚拟机
        {
            //先判断能否放入该虚拟机
            if (sv->usedCpuA + vmcur->core <= sv->core / 2 && sv->usedMemA + vmcur->mem <= sv->mem / 2) //当前服务器A节点可容纳虚拟机
            {
                if (&*sv != vmcur->svm.first)
                {
                    svnext = &*sv;
                    nodenext = 'A';
                    break;
                }
            }
            if (sv->usedCpuB + vmcur->core <= sv->core / 2 && sv->usedMemB + vmcur->mem <= sv->mem / 2)
            {
                if (&*sv != vmcur->svm.first)
                {
                    svnext = &*sv;
                    nodenext = 'B';
                    break;
                }
            }
        } //end of node == 0
        else if (vmcur->node == 1)
        {
            if (sv->usedCpuA + vmcur->core / 2 <= sv->core / 2 && sv->usedMemA + vmcur->mem / 2 <= sv->mem / 2 &&
                sv->usedCpuB + vmcur->core / 2 <= sv->core / 2 && sv->usedMemB + vmcur->mem / 2 <= sv->mem / 2) //当前服务器可容纳虚拟机
            {
                if (&*sv != vmcur->svm.first)
                {
                    svnext = &*sv;
                    nodenext = 'X';
                    break;
                }
            }
        }
    } //遍历服务器
    if (svnext != nullptr)
    {
        migvm2sv(vmcur, svnext, nodenext);
        return true;
    }
    else
        return false;
}

bool Datalib::migAvmNotEmpty(Vm *vmcur)
{
    Server *svnext = nullptr;
    char nodenext;
    for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++) //遍历服务器
    {
        if (vmcur->node == 0) //当前请求为单节点虚拟机
        {
            //先判断能否放入该虚拟机
            if (sv->usedCpuA + vmcur->core <= sv->core / 2 && sv->usedMemA + vmcur->mem <= sv->mem / 2 && !sv->empty()) //当前服务器A节点可容纳虚拟机
            {
                if (&*sv != vmcur->svm.first)
                {
                    svnext = &*sv;
                    nodenext = 'A';
                    break;
                }
            }
            if (sv->usedCpuB + vmcur->core <= sv->core / 2 && sv->usedMemB + vmcur->mem <= sv->mem / 2 && !sv->empty())
            {
                if (&*sv != vmcur->svm.first)
                {
                    svnext = &*sv;
                    nodenext = 'B';
                    break;
                }
            }
        } //end of node == 0
        else if (vmcur->node == 1)
        {
            if (sv->usedCpuA + vmcur->core / 2 <= sv->core / 2 && sv->usedMemA + vmcur->mem / 2 <= sv->mem / 2 &&
                sv->usedCpuB + vmcur->core / 2 <= sv->core / 2 && sv->usedMemB + vmcur->mem / 2 <= sv->mem / 2 && !sv->empty()) //当前服务器可容纳虚拟机
            {
                if (&*sv != vmcur->svm.first)
                {
                    svnext = &*sv;
                    nodenext = 'X';
                    break;
                }
            }
        }
    } //遍历服务器
    if (svnext != nullptr)
    {
        migvm2sv(vmcur, svnext, nodenext);
        return true;
    }
    else
        return false;
}

bool Datalib::migAvmFront(Vm *vmcur)
{
    int svmRank = vmcur->svm.first->rank;
    Server *svnext = nullptr;
    char nodenext;
    for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++) //遍历服务器
    {
        if ((sv->rank) >= svmRank)
            break;
        if (vmcur->node == 0) //当前请求为单节点虚拟机
        {
            //先判断能否放入该虚拟机
            if (sv->usedCpuA + vmcur->core <= sv->core / 2 && sv->usedMemA + vmcur->mem <= sv->mem / 2 && !sv->empty()) //当前服务器A节点可容纳虚拟机
            {
                if (&*sv != vmcur->svm.first)
                {
                    svnext = &*sv;
                    nodenext = 'A';
                    break;
                }
            }
            if (sv->usedCpuB + vmcur->core <= sv->core / 2 && sv->usedMemB + vmcur->mem <= sv->mem / 2 && !sv->empty())
            {
                if (&*sv != vmcur->svm.first)
                {
                    svnext = &*sv;
                    nodenext = 'B';
                    break;
                }
            }
        } //end of node == 0
        else if (vmcur->node == 1)
        {
            if (sv->usedCpuA + vmcur->core / 2 <= sv->core / 2 && sv->usedMemA + vmcur->mem / 2 <= sv->mem / 2 &&
                sv->usedCpuB + vmcur->core / 2 <= sv->core / 2 && sv->usedMemB + vmcur->mem / 2 <= sv->mem / 2 &&
                !sv->empty()) //当前服务器可容纳虚拟机
            {
                if (&*sv != vmcur->svm.first)
                {
                    svnext = &*sv;
                    nodenext = 'X';
                    break;
                }
            }
        }
    } //遍历服务器
    if (svnext != nullptr)
    {
        migvm2sv(vmcur, svnext, nodenext);
        return true;
    }
    else
        return false;
}

void Datalib::migvm2sv(Vm *vmmig, Server *sv, char node)
{
    delformig(vmmig); //将虚拟机从源服务器删除
    addvmformig(vmmig, sv, node);
}
void Datalib::delformig(Vm *vmmig)
{
    Server *sv = vmmig->svm.first;
    char nodetype = vmmig->svm.second;
    sv->cpurate -= double(vmmig->core) / sv->core;
    sv->memrate -= double(vmmig->mem) / sv->mem;
    if (nodetype == 'X') //如果此vm是双节点
    {
        sv->usedCpuA -= vmmig->core / 2;
        sv->usedMemA -= vmmig->mem / 2;
        sv->usedCpuB -= vmmig->core / 2;
        sv->usedMemB -= vmmig->mem / 2;
    }
    else
    {
        if (nodetype == 'A')
        {
            sv->usedCpuA -= vmmig->core;
            sv->usedMemA -= vmmig->mem;
        }
        else
        {
            sv->usedCpuB -= vmmig->core;
            sv->usedMemB -= vmmig->mem;
        }
    }

#if 1
    if (sv->usedCpuA < 0 || sv->usedCpuB < 0 || sv->usedMemA < 0 || sv->usedMemB < 0)
        std::cout << "ERROR: Delformig: server source < 0!" << std::endl;
#endif

    for (auto itv = sv->vms.begin(); itv != sv->vms.end(); itv++) //将此vm在数据库服务器保存的vms中删除
    {
        if ((*itv).first == vmmig->vmId)
        {
            sv->vms.erase(itv);
            break;
        }
    }
    for (auto vm = sv->vmMembers.begin(); vm != sv->vmMembers.end(); vm++)
    {
        if ((*vm)->vmId == vmmig->vmId) //两个地址不能比较相等
        {
            sv->vmMembers.erase(vm);
            break;
        }
    }
    //sv->vmMembers.remove(vmmig);//error:不能删除vmmig指针
}
void Datalib::addvmformig(Vm *vmmig, Server *sv, const char nodetype) //添加虚拟机到服务器
{
    sv->TurnOnflag = 1; //for test
    migNum++;
    vmmig->svm = {sv, nodetype}; //定义该虚拟机对应的服务器
    sv->vms.push_back({vmmig->vmId, nodetype});
    sv->vmMembers.insert(vmmig); //将vmmig添加的服务器的子虚拟机中
    sv->cpurate += double(vmmig->core) / sv->core;
    sv->memrate += double(vmmig->mem) / sv->mem;
    if (nodetype == 'X')
    {
        sv->usedCpuA += vmmig->core / 2;
        sv->usedMemA += vmmig->mem / 2;
        sv->usedCpuB += vmmig->core / 2;
        sv->usedMemB += vmmig->mem / 2;
        logmigMessage.push_back({vmmig->vmId, {sv, 'X'}});
    }
    if (nodetype == 'A')
    {
        sv->usedCpuA += vmmig->core;
        sv->usedMemA += vmmig->mem;
        logmigMessage.push_back({vmmig->vmId, {sv, 'A'}});
    }
    if (nodetype == 'B')
    {
        sv->usedCpuB += vmmig->core;
        sv->usedMemB += vmmig->mem;
        logmigMessage.push_back({vmmig->vmId, {sv, 'B'}});
    }
#if 1
    if (sv->usedCpuA > sv->core / 2 || sv->usedCpuB > sv->core / 2 || sv->usedMemA > sv->mem / 2 || sv->usedMemB > sv->mem / 2)
        cout << "ERROR: Resource overflow" << '\n';
#endif
}

void Datalib::purchasebyuser(Vmbase *base, int vmId, Serverbase *serverLib, int serverNum, int remainDays, double rate)
{
    ++svnum;
    Vm *newVm = new Vm(base); //按照用户需求添加一台虚拟机
    double minCost = INF;
    double costTot;
    Serverbase *svp = serverLib; //候选服务器
    int memUable = 0, cpuUable = 0;
    if (newVm->node == 1) //请求的虚拟机为双节点
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++) //遍历选出服务器
        {
            svt = serverLib + i;
            if (svt->core >= newVm->core && svt->mem >= newVm->mem)
            {
                //计算成本
                // if (remainDays > 500)
                // {
                cpuUable = std::min(svt->core, int((svt->mem) * rate));
                memUable = std::min(svt->mem, int((svt->core) / rate));

                costTot = double(svt->hdcost + svt->elecost * remainDays) / (cpuUable + memUable);
                //costTot = double(svt->hdcost + svt->elecost * remainDays) / (svt->core + svt->mem);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverTar->TurnOnflag = 1;                    //for test
        serverTar->rank = svnum;                      //服务器购买顺序
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'X'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    else
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++)
        {
            svt = serverLib + i;
            if (svt->core / 2 >= newVm->core && svt->mem / 2 >= newVm->mem)
            {
                //计算成本
                // if (remainDays > 500)
                // {
                cpuUable = std::min(svt->core, int((svt->mem) * rate));
                memUable = std::min(svt->mem, int((svt->core) / rate));
                // }
                // else
                // {
                //     double totcpurate = double(usedCpu) / totCpu;
                //     double totmemrate = double(usedMem) / totMem;
                //     cpuUable = svt->core * totcpurate;
                //     memUable = svt->mem * totmemrate;
                // }
                costTot = double(svt->hdcost + svt->elecost * remainDays) / (cpuUable + memUable);
                //costTot = double(svt->hdcost + svt->elecost * remainDays) / (svt->core + svt->mem);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverTar->TurnOnflag = 1;                    //for test
        serverTar->rank = svnum;                      //服务器购买顺序
        serverArray.push_back(*serverTar);            //创建数据库服务器
        psa.push_back(&serverArray.back());           //储存当天购买的服务器地址
        addvm(newVm, vmId, &serverArray.back(), 'A'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    totCpu += svp->core;
    totMem += svp->mem;
    ++purNum;
}

void Datalib::purchasebyuserToday(Req *req, Serverbase *serverLib, int serverNum, int remainDays, double rate)
{
    ++svnum;
    Vmbase *base = vmNameArray[req->vmName];
    int vmId = req->vmId;
    Vm *newVm = new Vm(base);
    double minCost = INF;
    double costTot;
    Serverbase *svp = serverLib;
    int memUable = 0, cpuUable = 0;
    if (newVm->node == 1) //请求的虚拟机为双节点
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++)
        {
            svt = serverLib + i;
            if (svt->core >= newVm->core && svt->mem >= newVm->mem)
            {
                //计算成本
                cpuUable = std::min(svt->core, int((svt->mem) * rate));
                memUable = std::min(svt->mem, int((svt->core) / rate));
                costTot = double(svt->hdcost + svt->elecost * remainDays) / (cpuUable + memUable);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverTar->TurnOnflag = 1;
        serverTar->rank = svnum;                                       //保存服务器购买的次序
        newServerToday.push_back(*serverTar);                          //创建数据库服务器
        psa.push_back(&newServerToday.back());                         //储存当天购买的服务器地址
        req->ack = addvmNew(newVm, vmId, &newServerToday.back(), 'X'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    else
    {
        Serverbase *svt = serverLib;
        for (int i = 0; i < serverNum; i++)
        {
            svt = serverLib + i;
            if (svt->core / 2 >= newVm->core && svt->mem / 2 >= newVm->mem)
            {
                cpuUable = std::min(svt->core, int((svt->mem) * rate));
                memUable = std::min(svt->mem, int((svt->core) / rate));
                costTot = double(svt->hdcost + svt->elecost * remainDays) / (cpuUable + memUable);
                if (costTot < minCost)
                {
                    svp = svt;
                    minCost = costTot;
                }
            }
        }
        Server *serverTar = new Server(svp);
        serverTar->TurnOnflag = 1;
        serverTar->rank = svnum;                                       //服务器购买顺序
        newServerToday.push_back(*serverTar);                          //创建数据库服务器
        psa.push_back(&newServerToday.back());                         //储存当天购买的服务器地址
        req->ack = addvmNew(newVm, vmId, &newServerToday.back(), 'A'); //将虚拟机添加到该服务器
        logpurMessage.push_back(serverTar->name);
    }
    totCpu += svp->core;
    totMem += svp->mem;
    ++purNum;
}

void Datalib::balance()
{
    bool miglim = 0;
    //从后向前遍历服务器，迁移使用率不平衡的服务器中的所有虚拟机
    for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++)
    {
        double svratedelta = (sv->memrate - sv->cpurate) > 0 ? (sv->memrate - sv->cpurate)
                                                             : -1 * (sv->memrate - sv->cpurate);
        double svrateaver = sv->memrate + sv->cpurate;
        if (svratedelta >= THRESHOLDDELTA && svrateaver >= THRESHOLDAVER)
        {
            int migcnt = 0;
            int rank = 0; //rank表示迁移不成功的数目
            int miglimit = sv->vms.size();
            while (migcnt < miglimit) //依次进行迁移
            {
                miglim = (migNum >= int(vmnumToday * 0.01)); //迁移数目达到上限
                if (miglim)
                    return;
                //遍历svright中的虚拟机，并逐个迁移
                int vmmigID = sv->vms[rank].first;
                Vm *vmmig = vmMap[vmmigID];
                if (!migAvmNotEmpty(vmmig)) //迁移当前虚拟机不成功
                    ++rank;
                ++migcnt;
            }
        }
    }
}
//}

void Datalib::foutUseRate(std::ofstream &out)
{
    double totcpurate = double(usedCpu) / totCpu;
    double totmemrate = double(usedMem) / totMem;
    double totrate = (totcpurate + totmemrate) / 2;
    out << "totalCpuRate = " << totcpurate;
    out << ", totalMemRate = " << totmemrate;
    out << ", averTotalHdRate = " << totrate << '\n';
}

void Datalib::coutUseRate()
{
    double totcpurate = double(usedCpu) / totCpu;
    double totmemrate = double(usedMem) / totMem;
    double totrate = (totcpurate + totmemrate) / 2;
    cout << "totalCpuRate = " << totcpurate;
    cout << ", totalMemRate = " << totmemrate;
    cout << ", averTotalHdRate = " << totrate << '\n';
}

void Datalib::mergevm()
{
    //计算当前数据中心服务器平均使用率
    double totcpurate = double(usedCpu) / totCpu;
    double totmemrate = double(usedMem) / totMem;
    double totrate = (totcpurate + totmemrate) / 2;
    if (totrate < THRESHOLD1)
    {
        //关闭一部分服务器：从后向前遍历服务器，迁移使用率小于阈值2的服务器中的所有虚拟机
        for (auto svright = serverArray.rbegin(); svright != serverArray.rend(); svright++)
        {
            double svrightrate = (svright->memrate + svright->cpurate) / 2;
            if (svrightrate < THRESHOLD2 && svrightrate > THRESHOLD3)
            {
                int migcnt = 0;
                int rank = 0; //rank表示迁移不成功的数目
                int miglimit = svright->vms.size();
                while (migcnt < miglimit) //依次进行迁移
                {
                    if (!migAble())
                        return;
                    //遍历svright中的虚拟机，并逐个迁移
                    int vmmigID = svright->vms[rank].first;
                    Vm *vmmig = vmMap[vmmigID];
                    if (!migAvmNotEmpty(vmmig)) //迁移当前虚拟机不成功
                        ++rank;
                    ++migcnt;
                }
            }
            else if (svrightrate < THRESHOLD3)
            {
                int migcnt = 0;
                int rank = 0; //rank表示迁移不成功的数目
                int miglimit = svright->vms.size();
                while (migcnt < miglimit) //依次进行迁移
                {
                    //miglim = (migNum >= vmnumToday / 34); //迁移数目达到上限
                    if (!migAble())
                        return;
                    //遍历svright中的虚拟机，并逐个迁移
                    int vmmigID = svright->vms[rank].first;
                    Vm *vmmig = vmMap[vmmigID];
                    if (!migAvmNotEmpty(vmmig)) //迁移当前虚拟机不成功
                        ++rank;
                    ++migcnt;
                }
            }
        }
    }
}

void Datalib::logUsedServernumber(std::ofstream &fout)
{
    int usedServerNumber = 0;
    for (auto it = serverArray.begin(); it != serverArray.end(); it++)
    {
        if (it->cpurate + it->memrate < 0.0001)
        {
            ++usedServerNumber;
        }
    }

    fout << "No Used Server NUmber:" << usedServerNumber << "\n";
}

bool Datalib::migAvm3(Vm *vmcur)
{
    for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++) //遍历服务器
    {
        if (&*sv != vmcur->svm.first) /*modify to sv rank < svm.first rank */
        {
            if (vmcur->node == 0) //当前请求为单节点虚拟机
            {
                //先判断能否放入该虚拟机
                if (sv->usedCpuA + vmcur->core <= sv->core / 2 && sv->usedMemA + vmcur->mem <= sv->mem / 2) //当前服务器A节点可容纳虚拟机
                {
                    migvm2sv(vmcur, &*sv, 'A');
                    return true;
                }
                if (sv->usedCpuB + vmcur->core <= sv->core / 2 && sv->usedMemB + vmcur->mem <= sv->mem / 2)
                {
                    migvm2sv(vmcur, &*sv, 'B');
                    return true;
                }
                for (auto vmSv = sv->vms.begin(); vmSv != sv->vms.end(); vmSv++)
                {
                    if (vmSv->second == 'A')
                    {
                        Vm *vmmig = vmMap[vmSv->first];
                        //如果交换后cpu和内存都不超过
                        if (sv->usedCpuA - vmmig->core + vmcur->core <= sv->core / 2 && sv->usedMemA - vmmig->mem + vmcur->mem <= sv->mem / 2)
                        {
                            //如果移进来的vmcur是A节点
                            if (vmcur->svm.second == 'A')
                            {
                                //如果vmmig移过去不会超过服务器空间
                                if (vmcur->svm.first->usedCpuA + vmmig->core <= vmcur->svm.first->core / 2 && vmcur->svm.first->usedMemA + vmmig->mem <= vmcur->svm.first->mem / 2)
                                {
                                    if (vmcur->core - vmmig->core + vmcur->mem - vmmig->mem > 0)
                                    {
                                        mig2mig(vmmig, vmcur);
                                        return true;
                                    }
                                }
                            }
                            if (vmcur->svm.second == 'B')
                            {
                                if (vmcur->svm.first->usedCpuB + vmmig->core <= vmcur->svm.first->core / 2 && vmcur->svm.first->usedMemB + vmmig->mem < vmcur->svm.first->mem / 2)
                                {
                                    if (vmcur->core - vmmig->core + vmcur->mem - vmmig->mem > 0)
                                    {
                                        mig2mig(vmmig, vmcur);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                    if (vmSv->second == 'B')
                    {
                        Vm *vmmig = vmMap[vmSv->first];
                        if (sv->usedCpuB - vmmig->core + vmcur->core <= sv->core / 2 && sv->usedMemB - vmmig->mem + vmcur->mem <= sv->mem / 2)
                        {
                            if (vmcur->svm.second == 'A')
                            {
                                if (vmcur->svm.first->usedCpuA + vmmig->core <= vmcur->svm.first->core / 2 && vmcur->svm.first->usedMemA + vmmig->mem <= vmcur->svm.first->mem / 2)
                                {
                                    if (vmcur->core - vmmig->core + vmcur->mem - vmmig->mem > 0)
                                    {
                                        mig2mig(vmmig, vmcur);
                                        return true;
                                    }
                                }
                            }
                            if (vmcur->svm.second == 'B')
                            {
                                if (vmcur->svm.first->usedCpuB + vmmig->core <= vmcur->svm.first->core / 2 && vmcur->svm.first->usedMemB + vmmig->mem <= vmcur->svm.first->mem / 2)
                                {
                                    if (vmcur->core - vmmig->core + vmcur->mem - vmmig->mem > 0)
                                    {
                                        mig2mig(vmmig, vmcur);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (vmcur->node == 1)
            {
                if (sv->usedCpuA + vmcur->core / 2 <= sv->core / 2 && sv->usedMemA + vmcur->mem / 2 <= sv->mem / 2 &&
                    sv->usedCpuB + vmcur->core / 2 <= sv->core / 2 && sv->usedMemB + vmcur->mem / 2 <= sv->mem / 2) //当前服务器可容纳虚拟机
                {
                    migvm2sv(vmcur, &*sv, 'X');
                    return true;
                }
                for (auto vmSv = sv->vms.begin(); vmSv != sv->vms.end(); vmSv++)
                {
                    if (vmSv->second == 'X')
                    {
                        Vm *vmmig = vmMap[vmSv->first];
                        if (sv->usedCpuA - vmmig->core / 2 + vmcur->core / 2 <= sv->core / 2 && sv->usedMemA - vmmig->mem / 2 + vmcur->mem / 2 <= sv->mem / 2 &&
                            sv->usedCpuB - vmmig->core / 2 + vmcur->core / 2 <= sv->core / 2 && sv->usedMemB - vmmig->mem / 2 + vmcur->mem / 2 <= sv->mem / 2)
                        {
                            if (vmcur->svm.first->usedCpuA + vmmig->core / 2 <= vmcur->svm.first->core / 2 && vmcur->svm.first->usedMemA + vmmig->mem / 2 <= vmcur->svm.first->mem / 2 &&
                                vmcur->svm.first->usedCpuB + vmmig->core / 2 <= vmcur->svm.first->core / 2 && vmcur->svm.first->usedMemB + vmmig->mem / 2 <= vmcur->svm.first->mem / 2)
                            {
                                if (vmcur->core - vmmig->core + vmcur->mem - vmmig->mem > 0)
                                {
                                    mig2mig(vmmig, vmcur);
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
    } //遍历服务器
    return false;
}

void Datalib::mig2mig(Vm *vmmig, Vm *vmcur)
{
    Server *serverTemp = vmmig->svm.first;
    char charTemp = vmmig->svm.second;
    delformig(vmmig);
    addvmformig(vmmig, vmcur->svm.first, vmcur->svm.second);
    delformig(vmcur);
    addvmformig(vmcur, serverTemp, charTemp);
}

void Datalib::printTotalCost()
{
    calTotalHdcost();
    cout << "totalHdcost: " << totalHdcost << ", totalElcost:" << totalElcost << '\n';
    cout << "totalcost:" << totalHdcost + totalElcost << '\n';
}
void Datalib::printTotalCost(std::ofstream &out)
{
    calTotalHdcost();
    out << "totalHdcost: " << totalHdcost << ", totalElcost:" << totalElcost << '\n';
    out << "totalcost:" << totalHdcost + totalElcost << '\n';
}

void Datalib::calTotalHdcost()
{
    totalHdcost = 0;
    for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++)
    {
        totalHdcost += sv->hdcost;
    }
}
void Datalib::calElcostOneday()
{
    for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++)
    {
        if (sv->TurnOnflag)
            totalElcost += sv->elecost;
    }
}

void Datalib::ifTurnOn()
{
    for (auto sv = serverArray.begin(); sv != serverArray.end(); sv++)
    {
        if (!sv->vms.empty())
            sv->TurnOnflag = 1;
        else
            sv->TurnOnflag = 0;
    }
}

bool Datalib::migAble()
{
    return migNum < int(vmnumToday * 0.03);
}

void Datalib::migrateTailToHead(int mode)
{

    double totcpurate = double(usedCpu) / totCpu;
    double totmemrate = double(usedMem) / totMem;
    double rateUseTot = (totcpurate + totmemrate) / 2; //当前数据库中服务器的平均使用率
    if (rateUseTot < THRESHOLD1)
    {
        for (auto itSvR = serverArray.rbegin(); itSvR != serverArray.rend(); itSvR++)
        {
            if (!migAble()) //迁移数目达到上限
                break;
            double svUsage = (itSvR->memrate + itSvR->cpurate) / 2; //得到服务器的使用率
            if (svUsage < THRESHOLD2)
            {
                for (auto VmR = (*itSvR).vmMembers.begin(); VmR != (*itSvR).vmMembers.end();)
                {
                    if (!migAble())
                        break;
                    bool migSuccess = 0;
                    for (auto itSvL = serverArray.begin(); itSvL != serverArray.end(); itSvL++)
                    {
                        if (&(*itSvL) != &(*itSvR) && !(itSvL->empty()))
                        {
                            if (mode == 1)
                            {
                                if (tryMigrateVmToSv(VmR, &(*itSvL))) //把VmR迁到*itSVL成功
                                {
                                    migSuccess = 1;
                                    break;
                                }
                            }
                            else if (mode == 2)
                            {
                                if (tryMigrateVmToSvSecond(VmR, &(*itSvL)))
                                {
                                    migSuccess = 1;
                                    break;
                                }
                            }
                        }
                        else
                            break;
                    }
                    if (!migSuccess)
                        VmR++;
                }
            }
        }
    }
}

bool Datalib::tryMigrateVmToSv(std::multiset<Vm *>::iterator &VmR, Server *sv)
{
    //cout << "TryMigrateVmTosv" << '\n';
    char nodetype = sv->ableToadd(*VmR);
    if (nodetype != 0)
    {
        Vm *vmmig = *(VmR++);
        migvm2sv(vmmig, sv, nodetype);
        return true;
    }
    else
        return false;
}

bool Datalib::tryMigrateVmToSvSecond(std::multiset<Vm *>::iterator &VmR, Server *sv)
{
    char nodetype = sv->ableToadd(*VmR);
    if (nodetype == 'X' || nodetype == 'A' || nodetype == 'B')
    {
        migvm2sv(*(VmR++), sv, nodetype);
        return true;
    }
    else if ((*VmR)->node == 0) //当前是单节点
    {
        if (rundays % 10 != 0)
            return false;
        Vm *vmmig = (*VmR);
        int deep = 0;
        for (auto vm = sv->vmMembers.begin(); vm != sv->vmMembers.end(); vm++, deep++)
        {
            if (deep >= 1)
                break;
            Vm *vmswap = *vm;
            char nodetype = vmswap->svm.second;
            if (nodetype == 'A' && vmswap < vmmig &&
                vmmig->core + sv->usedCpuA - vmswap->core <= sv->core / 2 &&
                vmmig->mem + sv->usedMemA - vmswap->mem <= sv->mem / 2)
            {
                if (migAvmNotEmpty(vmswap))
                {
                    VmR++;
                    migvm2sv(vmmig, sv, 'A');
                    return true;
                    VmR--;
                }
            }
            if (nodetype == 'B' && vmswap < vmmig &&
                vmmig->core + sv->usedCpuB - vmswap->core <= sv->core / 2 &&
                vmmig->mem + sv->usedMemB - vmswap->mem <= sv->mem / 2)
            {
                if (migAvmNotEmpty(vmswap))
                {
                    VmR++;
                    migvm2sv(vmmig, sv, 'B');
                    return true;
                    VmR--;
                }
            }
            if (nodetype == 'X' && vmswap < vmmig &&
                vmmig->core + sv->usedCpuA - vmswap->core / 2 <= sv->core / 2 &&
                vmmig->mem + sv->usedMemA - vmswap->mem / 2 <= sv->mem / 2)
            {
                if (migAvmNotEmpty(vmswap))
                {
                    VmR++;
                    migvm2sv(vmmig, sv, 'A');
                    return true;
                    VmR--;
                }
            }
        }
    }
    else
    {
        if (rundays % 10 != 0)
            return false;
        int deep = 0;
        for (auto vm = sv->vmMembers.begin(); vm != sv->vmMembers.end(); vm++, deep++)
        {
            if (deep >= 1)
                break;
            Vm *vmswap = *vm;
            Vm *vmmig = *VmR;
            char nodetype = vmswap->svm.second;
            if (nodetype == 'X' && vmswap < vmmig &&
                vmmig->core / 2 + sv->usedCpuA - vmswap->core / 2 <= sv->core / 2 &&
                vmmig->mem / 2 + sv->usedMemA - vmswap->mem / 2 <= sv->mem / 2 &&
                vmmig->core / 2 + sv->usedCpuB - vmswap->core / 2 <= sv->core / 2 &&
                vmmig->mem / 2 + sv->usedMemB - vmswap->mem / 2 <= sv->mem / 2)
            {

                if (migAvmNotEmpty(vmswap))
                {
                    VmR++;
                    migvm2sv(vmmig, sv, 'X');
                    return true;
                    VmR--;
                }
            }
            if (nodetype == 'A' && vmswap < vmmig &&
                vmmig->core / 2 + sv->usedCpuA - vmswap->core <= sv->core / 2 &&
                vmmig->mem / 2 + sv->usedMemA - vmswap->mem <= sv->mem / 2 &&
                vmmig->core / 2 + sv->usedCpuB <= sv->core / 2 &&
                vmmig->mem / 2 + sv->usedMemB <= sv->mem / 2)
            {
                if (migAvmNotEmpty(vmswap))
                {
                    VmR++;
                    migvm2sv(vmmig, sv, 'X');
                    return true;
                    VmR--;
                }
            }

            if (nodetype == 'B' && vmswap < vmmig &&
                vmmig->core / 2 + sv->usedCpuA <= sv->core / 2 &&
                vmmig->mem / 2 + sv->usedMemA <= sv->mem / 2 &&
                vmmig->core / 2 + sv->usedCpuB - vmswap->core <= sv->core / 2 &&
                vmmig->mem / 2 + sv->usedMemB - vmswap->mem <= sv->mem / 2)
            {
                if (migAvmNotEmpty(vmswap))
                {
                    VmR++;
                    migvm2sv(vmmig, sv, 'X');
                    return true;
                }
            }
        }
    }
    return false;
}

//返回虚拟机添加去的服务器的地址和节点
std::pair<Server *, char> Datalib::addvmNew(Vm *newVm, int vmId, Server *sv, const char nodetype)
{
    sv->TurnOnflag = 1;
    usedCpu += newVm->core;
    usedMem += newVm->mem;
    sv->cpurate += double(newVm->core) / sv->core;
    sv->memrate += double(newVm->mem) / sv->mem;
    vmnum++;
    newVm->vmId = vmId;
    newVm->svm = {sv, nodetype};
    sv->vms.push_back({vmId, nodetype});
    sv->vmMembers.insert(newVm);
    vmArray.push_back(*newVm);
    //vmMap.insert({vmid, &vmArray.back()})//这行代码会触发bug
    vmMap.insert({vmId, newVm});

    //目标服务器添加虚拟机后
    //增加其已使用的资源量
    if (nodetype == 'X')
    {
        sv->usedCpuA += newVm->core / 2;
        sv->usedMemA += newVm->mem / 2;
        sv->usedCpuB += newVm->core / 2;
        sv->usedMemB += newVm->mem / 2;
    }
    else
    {
        if (nodetype == 'A')
        {
            sv->usedCpuA += newVm->core;
            sv->usedMemA += newVm->mem;
        }
        else
        {
            sv->usedCpuB += newVm->core;
            sv->usedMemB += newVm->mem;
        }
    }
    return {sv, nodetype};
}

bool ReqCompare::operator()(const Req *reqL, const Req *reqR)
{
    bool greater;
    Vmbase *vmL = Datalib::vmNameArray[reqL->vmName];
    Vmbase *vmR = Datalib::vmNameArray[reqR->vmName];
    greater = (vmL->core + vmL->mem) > (vmR->core + vmR->mem);
    return greater;
}